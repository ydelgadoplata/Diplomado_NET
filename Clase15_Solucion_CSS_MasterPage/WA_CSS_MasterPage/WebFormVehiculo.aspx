﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormVehiculo.aspx.cs" Inherits="WA_CSS_MasterPage.WebFormVehiculo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="padding: 5px">
        <tr>
            <td colspan="2">
                <asp:ValidationSummary runat="server" ForeColor="Teal" DisplayMode="BulletList" ShowMessageBox="true" ShowSummary="false" HeaderText="Listado de errores" />
            </td>
        </tr>
        <tr>
            <td>Placa</td>
            <td>
                <asp:TextBox runat="server" ID="txtPlaca" MaxLength="6" />
                <asp:RequiredFieldValidator ErrorMessage="Campo requerido" ControlToValidate="txtPlaca" runat="server" ForeColor="Teal" ID="rfvPlaca">!</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ErrorMessage="Formato incorrecto" ControlToValidate="txtPlaca" runat="server" ValidationExpression="[a-zA-Z]{3}[0-9]{3}" ForeColor="Teal">!</asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>Confirmar Placa:</td>
            <td>
                <asp:TextBox runat="server" ID="txtConfirmarPlaca" MaxLength="6"/>
                <asp:RequiredFieldValidator ErrorMessage="Campo requerido" ControlToValidate="txtConfirmarPlaca" runat="server" ForeColor="Teal" ID="rfvConfirmarPlaca">!</asp:RequiredFieldValidator>
                <asp:CompareValidator ErrorMessage="La placa no coincide" ControlToValidate="txtConfirmarPlaca" ControlToCompare="txtPlaca" ForeColor="Teal" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Observación</td>
            <td>
                <asp:TextBox runat="server" ID="txtObservacion" MaxLength="200" Height="50" TextMode="MultiLine"/>
                <asp:RegularExpressionValidator ErrorMessage="Formato incorrecto" ControlToValidate="txtObservacion" runat="server" ValidationExpression="^[a-zA-ZÑñ]+(([\'\,\.\- ][a-zA-ZÑn ])?[a-zA-ZÑñ]*)*$" ForeColor="Teal">!</asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>Fecha</td>
            <td>
                <asp:TextBox runat="server" ID="txtFecha" />
                <asp:RegularExpressionValidator ErrorMessage="Formato incorrecto" ControlToValidate="txtFecha" runat="server" ValidationExpression="^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$" ForeColor="Teal">!</asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button Text="Guardar" ID="btnGuardar" runat="server" OnClick="btnGuardar_Click" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Label ID="lblMensaje" runat="server" />
            </td>
        </tr>
    </table>



</asp:Content>
