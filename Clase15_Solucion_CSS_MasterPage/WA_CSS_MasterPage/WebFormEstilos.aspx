﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFormEstilos.aspx.cs" Inherits="WA_CSS_MasterPage.WebFormEstilos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <p>
            Parrafo sin estilo
        </p>
        <p style="padding: 0px; margin: 0px; border-width: 1px; border-color: #000000; color:darkblue; text-align:center; font-size:48px; font-style:oblique; border-top-style: 1; border-right-style: 1; border-bottom-style: 1; border-left-style: 1;">
            Con estilo
        </p>
    </form>
</body>
</html>
