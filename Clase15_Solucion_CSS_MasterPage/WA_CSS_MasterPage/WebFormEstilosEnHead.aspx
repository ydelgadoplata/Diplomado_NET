﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFormEstilosEnHead.aspx.cs" Inherits="WA_CSS_MasterPage.WebFormEstilosEnHead" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        body{
            background-color:#ffcc00;
        }

        h1{
            text-align:center;
            color:#007acc;
            font-family:'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;

        }

        p{
            font-family:'Trebuchet MS';
            font-size:20px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <h1>Pagina con CSS definido en Head</h1>
            <p>
                Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.
            </p>
        
    </form>
</body>
</html>
