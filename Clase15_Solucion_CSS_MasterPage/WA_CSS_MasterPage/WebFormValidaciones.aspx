﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormValidaciones.aspx.cs" Inherits="WA_CSS_MasterPage.WebFormValidaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <table style="padding: 5px">
        <tr>
            <td colspan="2">
                <asp:ValidationSummary ID="vsErrores" runat="server" ForeColor="Red" DisplayMode="BulletList" ShowMessageBox="true" ShowSummary="false" HeaderText="Listado de errores" />
            </td>
        </tr>
        <tr>
            <td>Nombre</td>
            <td>
                <asp:TextBox runat="server" ID="txtNombre" MaxLength="20" />
                <asp:RequiredFieldValidator ID="rfvNombre" runat="server" ForeColor="Red" ControlToValidate="txtNombre" ErrorMessage="Campo Nombre requerido!">!</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Apellido</td>
            <td>
                <asp:TextBox runat="server" ID="txtApellido" MaxLength="30" />
                <asp:RequiredFieldValidator ID="rfvApellido" runat="server" ForeColor="Red" ControlToValidate="txtApellido" ErrorMessage="Campo Apellido requerido!">!</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Nro Hijos:</td>
            <td>
                <asp:TextBox runat="server" ID="txthijos" MaxLength="5" />
                <asp:RequiredFieldValidator ID="rfvHijos" runat="server" ForeColor="Red" ControlToValidate="txtHijos" ErrorMessage="Campo hijos requerido!">!</asp:RequiredFieldValidator>
                <asp:CompareValidator ErrorMessage="Valor ingresado debe ser númerico!" ControlToValidate="txtHijos" ForeColor="Red" runat="server" Operator="DataTypeCheck" Type="Integer">!</asp:CompareValidator>
                <asp:RangeValidator ErrorMessage="Número entre 0 y 10" ControlToValidate="txthijos" runat="server" ForeColor="Red" Type="Integer" MinimumValue="0" MaximumValue="10" />
            </td>
        </tr>
        <tr>
            <td>Correo:</td>
            <td>
                <asp:TextBox runat="server" ID="txtCorreo" />
                <asp:RequiredFieldValidator ErrorMessage="Ingrese correo!" ControlToValidate="txtCorreo" runat="server" ForeColor="Red"/>
                <asp:RegularExpressionValidator ErrorMessage="Correo no válido" ControlToValidate="txtCorreo" runat="server" ID="revCorreo" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>Letras:</td>
            <td>
                <asp:TextBox runat="server" ID="txtLetras" />
                <asp:RegularExpressionValidator ErrorMessage="Sólo caracteres" ControlToValidate="txtLetras" runat="server" ID="revLetras" ForeColor="Red" ValidationExpression="^[A-Za-zÑñ]+$">*</asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>Contraseña:</td>
            <td>
                <asp:TextBox runat="server" ID="txtClave" TextMode="Password"/>
                <asp:RequiredFieldValidator ID="revClave" ErrorMessage="Debe ingresar clave" ForeColor="Red" ControlToValidate="txtClave" runat="server">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Confirmar:</td>
            <td>
                <asp:TextBox runat="server" ID="txtConfirmar" TextMode="Password"/>
                <asp:CompareValidator ErrorMessage="La clave no coincide" ControlToValidate="txtConfirmar" ControlToCompare="txtClave" forecolor="Red" runat="server">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button Text="Guardar" runat="server" ID="btnGuardar" OnClick="btnGuardar_Click" />
                <asp:Button Text="Cancelar" runat="server" ID="btnCancelar" OnClick="btnCancelar_Click" CausesValidation="false"/>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Label ID="lblMensaje" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
