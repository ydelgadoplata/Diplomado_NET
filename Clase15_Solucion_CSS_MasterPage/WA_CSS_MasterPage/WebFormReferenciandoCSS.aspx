﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFormReferenciandoCSS.aspx.cs" Inherits="WA_CSS_MasterPage.WebFormReferenciandoCSS" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Styles/estilo.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <h1>COLOQUE SU TITULO</h1>
        <%--<p>
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.<br />
            <br />
            Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl. Phasellus pede arcu, dapibus eu, fermentum et, dapibus sed, urna.
        </p>
        <br />
        <p class="punteado">egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl. Phasellus pede arcu, dapibus eu, fermentum et, dapibus sed, urna.</p>
         <br />
        <p class="mayusc">egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl. Phasellus pede arcu, dapibus eu, fermentum et, dapibus sed, urna.</p>
        <br />
        <p class="punteado">
        <p class="capital">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.</p></p>
        <p style="font-weight:bold">
        <p class="capital">yesid delgado plata</p></p>--%>

        <ul style="list-style: lower-greek">
            <li>C#</li>
            <li>Java/li>
            <li>HTML</li>
                <li>CSS</li>
        </ul>

        <table>
            <tr>
                <th>NOMBRE</th>
                <th>APELLIDO</th>
            </tr>
            <tr>
                <td>STEVE</td>
                <td>HYUGA</td>
            </tr>
            <tr>
                <td>OLIVER</td>
                <td>ATTOM</td>
            </tr>
        </table>

    </form>
</body>
</html>
