﻿using LogDutyLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DutyLogApp
{
    public partial class WF_Login : Form
    {
        FachadaUsers fachadaUsers = new FachadaUsers();



        private static WF_Login InstanciawF_Login;
        //WF_Login wF_Login = new WF_Login();
        public static WF_Login GetChildInstance()
        {
            if (InstanciawF_Login == null) //if not created yet, Create an instance
            {
                InstanciawF_Login = new WF_Login();

            }
            return InstanciawF_Login;  //just created or created earlier.Return it
        }

        public string loggedUser
        {
            get { return txtCorreo.Text; }
        }

        public WF_Login()
        {
            InitializeComponent();

        }


        private void btnLogIn_Click(object sender, EventArgs e)
        {
            try
            {
                if (fachadaUsers.ValidarUsuario(txtCorreo.Text, txtPass.Text))
                {
                    DL_Main dL_Main = new DL_Main();
                    WF_Login wF_Login = new WF_Login();

                    MessageBox.Show("Se ha logueado exitosamente", $"Bienvenido {loggedUser}", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    WF_Tasks wF_Tasks = WF_Tasks.GetChildInstance();
                    wF_Tasks.MdiParent = this.ParentForm;
                    wF_Tasks.Show();
                    wF_Tasks.StatusLogIn.Text = loggedUser;
                    wF_Tasks.BringToFront();
                    wF_Tasks.Dock = DockStyle.Fill;

                    
                }
                else
                {
                    MessageBox.Show("Contraseña no válida", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtPass.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtCorreo.Focus();
            }
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            DL_Main dL_Main = new DL_Main();
            WF_Login wF_Login = new WF_Login();
            //InstanciawF_Login.Hide();
            txtCorreo.Text = txtPass.Text = null;
            WF_RegistrarUsuario registrarUsuario = WF_RegistrarUsuario.GetChildInstance();
            registrarUsuario.MdiParent = this.ParentForm;
            registrarUsuario.Show();
            registrarUsuario.StartPosition = FormStartPosition.CenterParent;
        }

        private void WF_Login_Load(object sender, EventArgs e)
        {
            txtCorreo.Text = txtPass.Text = "";
        }
    }
}
