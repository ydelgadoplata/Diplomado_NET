﻿using DataAccess;
using LogDutyLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DutyLogApp
{
    public partial class WF_RegistrarUsuario : Form
    {
        FachadaUsers fachadaUsers = new FachadaUsers();

        private static WF_RegistrarUsuario InstanciawF_Registrar;
        //WF_Login wF_Login = new WF_Login();
        public static WF_RegistrarUsuario GetChildInstance()
        {
            if (InstanciawF_Registrar == null) //if not created yet, Create an instance
            {
                InstanciawF_Registrar = new WF_RegistrarUsuario();

            }
            return InstanciawF_Registrar;  //just created or created earlier.Return it
        }


        public WF_RegistrarUsuario()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            DL_Main dL_Main = new DL_Main();
            WF_RegistrarUsuario wF_RegistrarUsuario = new WF_RegistrarUsuario();
            
            txtApellidos.Text = txtContrasena.Text = txtCorreo.Text = txtNombre.Text = null;
            WF_RegistrarUsuario.InstanciawF_Registrar = null;
            this.Close();
            //WF_Login wF_Login = WF_Login.GetChildInstance();
            //wF_Login.MdiParent = this.ParentForm;
            //wF_Login.Dock = DockStyle.Fill;
            //wF_Login.Show();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtCorreo.Text))
            {
                Regex reg = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
                if (reg.IsMatch(txtCorreo.Text))
                {

                    Users Usuario = new Users
                    {
                        Nombre = txtNombre.Text,
                        Apellidos = txtApellidos.Text,
                        Correo = txtCorreo.Text,
                        Contrasena = txtContrasena.Text
                    };

                    if (fachadaUsers.IngresarUsuario(Usuario)==1)
                    {
                        MessageBox.Show("Usuario registrado con éxito", "Registro", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        txtNombre.Text = txtApellidos.Text = txtCorreo.Text = txtContrasena.Text = "";

                        WF_RegistrarUsuario.InstanciawF_Registrar = null;
                        this.Close(); 
                    }
                    else
                    {
                        MessageBox.Show("El usuario no pudo registrarse o está registrado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un correo válido", "Correo inválido", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar todos los campos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
    }
}
