﻿namespace DutyLogApp
{
    partial class WF_Tasks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.pnlBarraEstado = new System.Windows.Forms.Panel();
            this.StatusLogIn = new System.Windows.Forms.Label();
            this.pnlGrids = new System.Windows.Forms.Panel();
            this.grpBox_TskProg = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.grpBox_TskCompletadas = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.pnlTasks = new System.Windows.Forms.Panel();
            this.grpBox_Tarea = new System.Windows.Forms.GroupBox();
            this.lblTask = new System.Windows.Forms.Label();
            this.btnEjecutar = new System.Windows.Forms.Button();
            this.btnProgTsk = new System.Windows.Forms.Button();
            this.lblNotaTsk = new System.Windows.Forms.Label();
            this.txtNombreTsk = new System.Windows.Forms.TextBox();
            this.txtNotaTsk = new System.Windows.Forms.TextBox();
            this.lblFechaTsk = new System.Windows.Forms.Label();
            this.pckFechaTsk = new System.Windows.Forms.DateTimePicker();
            this.chkCompleteTsk = new System.Windows.Forms.CheckBox();
            this.pnlBlank = new System.Windows.Forms.Panel();
            this.lblIdTask = new System.Windows.Forms.Label();
            this.grpBox_TEjec = new System.Windows.Forms.GroupBox();
            this.lblHrs = new System.Windows.Forms.Label();
            this.lblMin = new System.Windows.Forms.Label();
            this.lblSeg = new System.Windows.Forms.Label();
            this.lblDospuntos = new System.Windows.Forms.Label();
            this.dospuntos_seg = new System.Windows.Forms.Label();
            this.pnl_WFTasks = new System.Windows.Forms.Panel();
            this.dutyLogDBDataSet = new DutyLogApp.DutyLogDBDataSet();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.pnlBarraEstado.SuspendLayout();
            this.pnlGrids.SuspendLayout();
            this.grpBox_TskProg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.grpBox_TskCompletadas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.pnlTasks.SuspendLayout();
            this.grpBox_Tarea.SuspendLayout();
            this.pnlBlank.SuspendLayout();
            this.grpBox_TEjec.SuspendLayout();
            this.pnl_WFTasks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dutyLogDBDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = this.dutyLogDBDataSet;
            this.bindingSource1.Position = 0;
            // 
            // pnlBarraEstado
            // 
            this.pnlBarraEstado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlBarraEstado.Controls.Add(this.StatusLogIn);
            this.pnlBarraEstado.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBarraEstado.Location = new System.Drawing.Point(0, 500);
            this.pnlBarraEstado.Name = "pnlBarraEstado";
            this.pnlBarraEstado.Size = new System.Drawing.Size(784, 25);
            this.pnlBarraEstado.TabIndex = 12;
            // 
            // StatusLogIn
            // 
            this.StatusLogIn.AutoSize = true;
            this.StatusLogIn.Location = new System.Drawing.Point(10, 6);
            this.StatusLogIn.Name = "StatusLogIn";
            this.StatusLogIn.Size = new System.Drawing.Size(78, 13);
            this.StatusLogIn.TabIndex = 0;
            this.StatusLogIn.Text = "No hay usuario";
            // 
            // pnlGrids
            // 
            this.pnlGrids.Controls.Add(this.grpBox_TskCompletadas);
            this.pnlGrids.Controls.Add(this.grpBox_TskProg);
            this.pnlGrids.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlGrids.Location = new System.Drawing.Point(444, 0);
            this.pnlGrids.Name = "pnlGrids";
            this.pnlGrids.Size = new System.Drawing.Size(340, 500);
            this.pnlGrids.TabIndex = 13;
            // 
            // grpBox_TskProg
            // 
            this.grpBox_TskProg.Controls.Add(this.dataGridView1);
            this.grpBox_TskProg.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpBox_TskProg.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBox_TskProg.Location = new System.Drawing.Point(0, 0);
            this.grpBox_TskProg.Name = "grpBox_TskProg";
            this.grpBox_TskProg.Size = new System.Drawing.Size(340, 250);
            this.grpBox_TskProg.TabIndex = 9;
            this.grpBox_TskProg.TabStop = false;
            this.grpBox_TskProg.Text = "Tareas Programadas";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 18);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(334, 229);
            this.dataGridView1.TabIndex = 0;
            // 
            // grpBox_TskCompletadas
            // 
            this.grpBox_TskCompletadas.Controls.Add(this.dataGridView2);
            this.grpBox_TskCompletadas.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grpBox_TskCompletadas.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBox_TskCompletadas.Location = new System.Drawing.Point(0, 256);
            this.grpBox_TskCompletadas.Name = "grpBox_TskCompletadas";
            this.grpBox_TskCompletadas.Size = new System.Drawing.Size(340, 244);
            this.grpBox_TskCompletadas.TabIndex = 10;
            this.grpBox_TskCompletadas.TabStop = false;
            this.grpBox_TskCompletadas.Text = "Tareas Completadas";
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 18);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(334, 223);
            this.dataGridView2.TabIndex = 0;
            // 
            // pnlTasks
            // 
            this.pnlTasks.Controls.Add(this.grpBox_TEjec);
            this.pnlTasks.Controls.Add(this.pnlBlank);
            this.pnlTasks.Controls.Add(this.grpBox_Tarea);
            this.pnlTasks.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlTasks.Location = new System.Drawing.Point(0, 0);
            this.pnlTasks.Name = "pnlTasks";
            this.pnlTasks.Size = new System.Drawing.Size(430, 500);
            this.pnlTasks.TabIndex = 14;
            // 
            // grpBox_Tarea
            // 
            this.grpBox_Tarea.Controls.Add(this.chkCompleteTsk);
            this.grpBox_Tarea.Controls.Add(this.pckFechaTsk);
            this.grpBox_Tarea.Controls.Add(this.lblFechaTsk);
            this.grpBox_Tarea.Controls.Add(this.txtNotaTsk);
            this.grpBox_Tarea.Controls.Add(this.txtNombreTsk);
            this.grpBox_Tarea.Controls.Add(this.lblNotaTsk);
            this.grpBox_Tarea.Controls.Add(this.btnProgTsk);
            this.grpBox_Tarea.Controls.Add(this.btnEjecutar);
            this.grpBox_Tarea.Controls.Add(this.lblTask);
            this.grpBox_Tarea.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpBox_Tarea.Location = new System.Drawing.Point(0, 0);
            this.grpBox_Tarea.Name = "grpBox_Tarea";
            this.grpBox_Tarea.Size = new System.Drawing.Size(430, 225);
            this.grpBox_Tarea.TabIndex = 11;
            this.grpBox_Tarea.TabStop = false;
            // 
            // lblTask
            // 
            this.lblTask.AutoSize = true;
            this.lblTask.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTask.Location = new System.Drawing.Point(7, 16);
            this.lblTask.Name = "lblTask";
            this.lblTask.Size = new System.Drawing.Size(87, 13);
            this.lblTask.TabIndex = 11;
            this.lblTask.Text = "Tarea/Actividad:";
            // 
            // btnEjecutar
            // 
            this.btnEjecutar.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnEjecutar.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnEjecutar.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEjecutar.ForeColor = System.Drawing.Color.Black;
            this.btnEjecutar.Image = global::DutyLogApp.Properties.Resources.Accept_icon;
            this.btnEjecutar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEjecutar.Location = new System.Drawing.Point(40, 180);
            this.btnEjecutar.Name = "btnEjecutar";
            this.btnEjecutar.Padding = new System.Windows.Forms.Padding(0, 0, 8, 0);
            this.btnEjecutar.Size = new System.Drawing.Size(105, 35);
            this.btnEjecutar.TabIndex = 13;
            this.btnEjecutar.Text = "  Ejecutar";
            this.btnEjecutar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEjecutar.UseVisualStyleBackColor = false;
            this.btnEjecutar.Click += new System.EventHandler(this.btnEjecutar_Click_1);
            // 
            // btnProgTsk
            // 
            this.btnProgTsk.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnProgTsk.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnProgTsk.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProgTsk.ForeColor = System.Drawing.Color.Black;
            this.btnProgTsk.Image = global::DutyLogApp.Properties.Resources.schedule_icon;
            this.btnProgTsk.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnProgTsk.Location = new System.Drawing.Point(180, 180);
            this.btnProgTsk.Name = "btnProgTsk";
            this.btnProgTsk.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.btnProgTsk.Size = new System.Drawing.Size(105, 35);
            this.btnProgTsk.TabIndex = 12;
            this.btnProgTsk.Text = "Programar";
            this.btnProgTsk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProgTsk.UseVisualStyleBackColor = false;
            this.btnProgTsk.Click += new System.EventHandler(this.btnProgTsk_Click);
            // 
            // lblNotaTsk
            // 
            this.lblNotaTsk.AutoSize = true;
            this.lblNotaTsk.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNotaTsk.Location = new System.Drawing.Point(7, 42);
            this.lblNotaTsk.Name = "lblNotaTsk";
            this.lblNotaTsk.Size = new System.Drawing.Size(35, 13);
            this.lblNotaTsk.TabIndex = 10;
            this.lblNotaTsk.Text = "Nota:";
            // 
            // txtNombreTsk
            // 
            this.txtNombreTsk.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreTsk.Location = new System.Drawing.Point(152, 11);
            this.txtNombreTsk.Name = "txtNombreTsk";
            this.txtNombreTsk.Size = new System.Drawing.Size(272, 22);
            this.txtNombreTsk.TabIndex = 15;
            // 
            // txtNotaTsk
            // 
            this.txtNotaTsk.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNotaTsk.Location = new System.Drawing.Point(152, 42);
            this.txtNotaTsk.Multiline = true;
            this.txtNotaTsk.Name = "txtNotaTsk";
            this.txtNotaTsk.Size = new System.Drawing.Size(272, 89);
            this.txtNotaTsk.TabIndex = 14;
            // 
            // lblFechaTsk
            // 
            this.lblFechaTsk.AutoSize = true;
            this.lblFechaTsk.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaTsk.Location = new System.Drawing.Point(7, 149);
            this.lblFechaTsk.Name = "lblFechaTsk";
            this.lblFechaTsk.Size = new System.Drawing.Size(105, 13);
            this.lblFechaTsk.TabIndex = 16;
            this.lblFechaTsk.Text = "Fecha Programada:";
            // 
            // pckFechaTsk
            // 
            this.pckFechaTsk.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pckFechaTsk.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.pckFechaTsk.Location = new System.Drawing.Point(152, 144);
            this.pckFechaTsk.Name = "pckFechaTsk";
            this.pckFechaTsk.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.pckFechaTsk.Size = new System.Drawing.Size(272, 22);
            this.pckFechaTsk.TabIndex = 17;
            // 
            // chkCompleteTsk
            // 
            this.chkCompleteTsk.AutoSize = true;
            this.chkCompleteTsk.Location = new System.Drawing.Point(320, 191);
            this.chkCompleteTsk.Name = "chkCompleteTsk";
            this.chkCompleteTsk.Size = new System.Drawing.Size(82, 17);
            this.chkCompleteTsk.TabIndex = 20;
            this.chkCompleteTsk.Text = "Completada";
            this.chkCompleteTsk.UseVisualStyleBackColor = true;
            this.chkCompleteTsk.CheckedChanged += new System.EventHandler(this.chkCompleteTsk_CheckedChanged);
            // 
            // pnlBlank
            // 
            this.pnlBlank.Controls.Add(this.lblIdTask);
            this.pnlBlank.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBlank.Location = new System.Drawing.Point(0, 356);
            this.pnlBlank.Name = "pnlBlank";
            this.pnlBlank.Size = new System.Drawing.Size(430, 144);
            this.pnlBlank.TabIndex = 12;
            // 
            // lblIdTask
            // 
            this.lblIdTask.AutoSize = true;
            this.lblIdTask.Location = new System.Drawing.Point(15, 39);
            this.lblIdTask.Name = "lblIdTask";
            this.lblIdTask.Size = new System.Drawing.Size(13, 13);
            this.lblIdTask.TabIndex = 0;
            this.lblIdTask.Text = "0";
            this.lblIdTask.Visible = false;
            // 
            // grpBox_TEjec
            // 
            this.grpBox_TEjec.Controls.Add(this.dospuntos_seg);
            this.grpBox_TEjec.Controls.Add(this.lblDospuntos);
            this.grpBox_TEjec.Controls.Add(this.lblSeg);
            this.grpBox_TEjec.Controls.Add(this.lblMin);
            this.grpBox_TEjec.Controls.Add(this.lblHrs);
            this.grpBox_TEjec.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpBox_TEjec.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBox_TEjec.Location = new System.Drawing.Point(0, 225);
            this.grpBox_TEjec.Name = "grpBox_TEjec";
            this.grpBox_TEjec.Size = new System.Drawing.Size(430, 131);
            this.grpBox_TEjec.TabIndex = 13;
            this.grpBox_TEjec.TabStop = false;
            this.grpBox_TEjec.Text = "Tiempo de ejecución";
            // 
            // lblHrs
            // 
            this.lblHrs.AutoSize = true;
            this.lblHrs.Font = new System.Drawing.Font("Segoe UI", 50.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHrs.Location = new System.Drawing.Point(29, 18);
            this.lblHrs.Name = "lblHrs";
            this.lblHrs.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblHrs.Size = new System.Drawing.Size(110, 89);
            this.lblHrs.TabIndex = 10;
            this.lblHrs.Text = "00";
            this.lblHrs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMin
            // 
            this.lblMin.AutoSize = true;
            this.lblMin.Font = new System.Drawing.Font("Segoe UI", 50.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMin.Location = new System.Drawing.Point(160, 18);
            this.lblMin.Name = "lblMin";
            this.lblMin.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblMin.Size = new System.Drawing.Size(110, 89);
            this.lblMin.TabIndex = 11;
            this.lblMin.Text = "00";
            this.lblMin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSeg
            // 
            this.lblSeg.AutoSize = true;
            this.lblSeg.Font = new System.Drawing.Font("Segoe UI", 50.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSeg.Location = new System.Drawing.Point(291, 18);
            this.lblSeg.Name = "lblSeg";
            this.lblSeg.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblSeg.Size = new System.Drawing.Size(110, 89);
            this.lblSeg.TabIndex = 12;
            this.lblSeg.Text = "00";
            this.lblSeg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDospuntos
            // 
            this.lblDospuntos.AutoSize = true;
            this.lblDospuntos.Font = new System.Drawing.Font("Segoe UI", 50.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDospuntos.Location = new System.Drawing.Point(123, 18);
            this.lblDospuntos.Name = "lblDospuntos";
            this.lblDospuntos.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDospuntos.Size = new System.Drawing.Size(53, 89);
            this.lblDospuntos.TabIndex = 13;
            this.lblDospuntos.Text = ":";
            this.lblDospuntos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dospuntos_seg
            // 
            this.dospuntos_seg.AutoSize = true;
            this.dospuntos_seg.Font = new System.Drawing.Font("Segoe UI", 50.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dospuntos_seg.Location = new System.Drawing.Point(254, 18);
            this.dospuntos_seg.Name = "dospuntos_seg";
            this.dospuntos_seg.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dospuntos_seg.Size = new System.Drawing.Size(53, 89);
            this.dospuntos_seg.TabIndex = 14;
            this.dospuntos_seg.Text = ":";
            this.dospuntos_seg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnl_WFTasks
            // 
            this.pnl_WFTasks.Controls.Add(this.pnlTasks);
            this.pnl_WFTasks.Controls.Add(this.pnlGrids);
            this.pnl_WFTasks.Controls.Add(this.pnlBarraEstado);
            this.pnl_WFTasks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_WFTasks.Location = new System.Drawing.Point(0, 0);
            this.pnl_WFTasks.Name = "pnl_WFTasks";
            this.pnl_WFTasks.Size = new System.Drawing.Size(784, 525);
            this.pnl_WFTasks.TabIndex = 12;
            // 
            // dutyLogDBDataSet
            // 
            this.dutyLogDBDataSet.DataSetName = "DutyLogDBDataSet";
            this.dutyLogDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // WF_Tasks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(784, 525);
            this.ControlBox = false;
            this.Controls.Add(this.pnl_WFTasks);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WF_Tasks";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WF_Tasks";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.WF_Tasks_FormClosed);
            this.Load += new System.EventHandler(this.WF_Tasks_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.pnlBarraEstado.ResumeLayout(false);
            this.pnlBarraEstado.PerformLayout();
            this.pnlGrids.ResumeLayout(false);
            this.grpBox_TskProg.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.grpBox_TskCompletadas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.pnlTasks.ResumeLayout(false);
            this.grpBox_Tarea.ResumeLayout(false);
            this.grpBox_Tarea.PerformLayout();
            this.pnlBlank.ResumeLayout(false);
            this.pnlBlank.PerformLayout();
            this.grpBox_TEjec.ResumeLayout(false);
            this.grpBox_TEjec.PerformLayout();
            this.pnl_WFTasks.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dutyLogDBDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn checklistDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn comentarioDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource bindingSource1;
        private DutyLogDBDataSet dutyLogDBDataSet;
        public System.Windows.Forms.Panel pnlBarraEstado;
        public System.Windows.Forms.Label StatusLogIn;
        private System.Windows.Forms.Panel pnlGrids;
        private System.Windows.Forms.GroupBox grpBox_TskCompletadas;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.GroupBox grpBox_TskProg;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel pnlTasks;
        private System.Windows.Forms.GroupBox grpBox_TEjec;
        private System.Windows.Forms.Label dospuntos_seg;
        private System.Windows.Forms.Label lblDospuntos;
        private System.Windows.Forms.Label lblSeg;
        private System.Windows.Forms.Label lblMin;
        private System.Windows.Forms.Label lblHrs;
        private System.Windows.Forms.Panel pnlBlank;
        private System.Windows.Forms.GroupBox grpBox_Tarea;
        private System.Windows.Forms.CheckBox chkCompleteTsk;
        private System.Windows.Forms.DateTimePicker pckFechaTsk;
        private System.Windows.Forms.Label lblFechaTsk;
        private System.Windows.Forms.TextBox txtNotaTsk;
        private System.Windows.Forms.TextBox txtNombreTsk;
        private System.Windows.Forms.Label lblNotaTsk;
        private System.Windows.Forms.Button btnProgTsk;
        private System.Windows.Forms.Button btnEjecutar;
        private System.Windows.Forms.Label lblTask;
        private System.Windows.Forms.Panel pnl_WFTasks;
        public System.Windows.Forms.Label lblIdTask;
    }
}