﻿using DataAccess;
using LogDutyLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DutyLogApp
{


    public partial class WF_Tasks : Form
    {
        private DL_Main DL_Main = new DL_Main();
        private WF_Login wF_Login = new WF_Login();

        private static WF_Tasks Instancia_wF_Tasks;

        int i = 0;
        int mm = 0;
        int hh = 0;
        int ss = 0;

        FachadaTareas _fachadaTareas = new FachadaTareas();
        Tasks tasks = new Tasks();

        public static WF_Tasks GetChildInstance()
        {
            if (Instancia_wF_Tasks == null) //if not created yet, Create an instance
            {
                Instancia_wF_Tasks = new WF_Tasks();

            }
            return Instancia_wF_Tasks;  //just created or created earlier.Return it
        }

        public WF_Tasks()
        {
            InitializeComponent();
        }



        private void WF_Tasks_Load(object sender, EventArgs e)
        {
            StatusLogIn.Text = wF_Login.loggedUser;
            //lblIdTask.Text = dutyLogDBDataSet.Tasks.IdTaskColumn.DefaultValue.ToString();
        }

        private void timer1_Tick(object sender, EventArgs e) //Contador de Tiempo
        {
            i++;

            //int ss = 0;
            //int mm = 0;
            //int hh = 0;

            if (i > 0 && i <= 9)
            {
                lblSeg.Text = "0" + i.ToString();
            }
            else
            {
                lblSeg.Text = i.ToString();
            }

            ss = i;

            if (ss == 60)
            {
                ss = i = 0;
                mm++;

                lblSeg.Text = "0" + ss.ToString();

                if (mm > 0 && mm <= 9)
                {
                    lblMin.Text = "0" + mm.ToString();
                }
                else
                {
                    lblMin.Text = mm.ToString();
                }
            }
            //else
            //    if (ss<60)
            // {

            //}

            if (mm == 60)
            {
                hh++;
                mm = 0;

                lblMin.Text = "0" + mm.ToString();

                if (mm > 0 && mm <= 9)
                {
                    lblHrs.Text = "0" + hh.ToString();
                }
                else
                {
                    lblHrs.Text = hh.ToString();
                }
            }
        }

        private void WF_Tasks_FormClosed(object sender, FormClosedEventArgs e) //Cerrando formulario
        {
            timer1.Stop();
            //int[] TEjec = new int[] { hh, mm, ss };
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnEjecutar_Click_1(object sender, EventArgs e)
        {
            if (timer1.Enabled)
            {
                try
                {
                    btnEjecutar.Text = "Ejecutar";
                    btnEjecutar.Image = DutyLogApp.Properties.Resources.Accept_icon;
                    timer1.Interval = 10;
                    timer1.Enabled = false;
                    txtNombreTsk.Enabled = txtNotaTsk.Enabled = pckFechaTsk.Enabled = chkCompleteTsk.Enabled = btnProgTsk.Enabled = true;

                    Tasks tasks = new Tasks
                    {
                        IdTask = Convert.ToInt32(lblIdTask.Text),
                        TaskTitle = txtNombreTsk.Text,
                        Descripcion = txtNotaTsk.Text,
                        F_Vencimiento = pckFechaTsk.Value,
                        T_Ejecucion = $"{hh}:{mm}:{ss}",
                        Completada = chkCompleteTsk.Checked,
                        IdUser = StatusLogIn.Text
                    };

                    if (_fachadaTareas.AddUpdtTarea(tasks) == 1)
                    {
                        MessageBox.Show("Tarea actualizada", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //Tasks task = _fachadaTareas.RetornarTarea(Convert.ToInt32(lblIdTask.Text));
                        //lblIdTask.Text = task.IdTask.ToString();

                    }
                    else
                    {
                        //MessageBox.Show("Tarea no actualizada", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //txtNombreTsk.Enabled = txtNotaTsk.Enabled = pckFechaTsk.Enabled = chkCompleteTsk.Enabled = btnProgTsk.Enabled = false;
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show($"{ex.Message}", "Error");
                }
            }
            else
            {

                try
                {

                    if (!String.IsNullOrEmpty(txtNombreTsk.Text))
                    {
                        //lblIdTask.Text = ;

                        Tasks tasks = _fachadaTareas.RetornarTarea(Convert.ToInt32(lblIdTask.Text));

                        
                            txtNombreTsk.Enabled = txtNotaTsk.Enabled = pckFechaTsk.Enabled = chkCompleteTsk.Enabled = btnProgTsk.Enabled = false;
                            btnEjecutar.Text = "Detener";
                            btnEjecutar.Image = DutyLogApp.Properties.Resources.Button_Close_icon;

                            timer1.Interval = 10;
                            timer1.Enabled = true;
                     
                    }
                    else
                    {
                        MessageBox.Show("Debe colocar el nombre de la tarea", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        timer1.Enabled = false;
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show($"{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
                
            }
        }

        private void btnProgTsk_Click(object sender, EventArgs e)
        {
            try
            {
                Tasks tasks = new Tasks
                {
                    IdTask = Convert.ToInt32(lblIdTask.Text),
                    TaskTitle = txtNombreTsk.Text,
                    Descripcion = txtNotaTsk.Text,
                    F_Vencimiento = pckFechaTsk.Value,
                    T_Ejecucion = $"{hh}:{mm}:{ss}",
                    Completada = chkCompleteTsk.Checked,
                    IdUser = StatusLogIn.Text
                };

                if (_fachadaTareas.InsertarTarea(tasks) == 1)
                {
                    MessageBox.Show("Tarea programada!", "OK");
                    lblHrs.Text = lblMin.Text = lblSeg.Text = "00";
                }
                else
                {
                    MessageBox.Show("No se pudo programar la tarea", "Error");
                }

                hh = mm = ss = 0;
                txtNombreTsk.Text = txtNotaTsk.Text = null;
                chkCompleteTsk.Checked = false;
                pckFechaTsk.Value = DateTime.Now.Date;
            }

            catch (Exception ex)
            {
                MessageBox.Show($"{ex.Message}", "Error");
            }

        }

        private void chkCompleteTsk_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCompleteTsk.Checked)
            {
                btnEjecutar.Enabled = btnProgTsk.Enabled = false;
            }
            else
            {
                btnEjecutar.Enabled = btnProgTsk.Enabled = true;
            }
        }
    }
}
