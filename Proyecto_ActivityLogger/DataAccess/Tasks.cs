//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tasks
    {
        public int IdTask { get; set; }
        public string IdUser { get; set; }
        public string TaskTitle { get; set; }
        public string Descripcion { get; set; }
        public Nullable<System.DateTime> F_Vencimiento { get; set; }
        public string T_Ejecucion { get; set; }
        public bool Completada { get; set; }
    
        public virtual Users Users { get; set; }
    }
}
