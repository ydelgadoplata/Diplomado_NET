﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;

namespace LogDutyLog
{
    public class FachadaTareas
    {
        private DutyLogDBEntities _contexto;

        public FachadaTareas()
        {
            _contexto = new DutyLogDBEntities();
        }

        public int InsertarTarea(Tasks tasks)
        {
            if (String.IsNullOrEmpty(tasks.TaskTitle))
            {
                throw new Exception("Debe colocar nombre a la Tarea");
            }

            _contexto.Tasks.Add(tasks);
            //_contexto.Entry(tasks).State = EntityState.Added;
            return _contexto.SaveChanges();
        }

        public int AddUpdtTarea(Tasks tasks)
        {
            if (String.IsNullOrEmpty(tasks.TaskTitle))
            {
                throw new Exception("Debe colocar nombre a la Tarea");
            }

            //_contexto.Entry(tasks).State = EntityState.Modified;
            _contexto.Entry(tasks).State = tasks.IdTask == 0 ?
                                   EntityState.Added :
                                   EntityState.Modified;
            return _contexto.SaveChanges();
        }

        public Tasks RetornarTarea(int idtask)

        {
            try
            {
                return _contexto.Tasks.Find(idtask);
            }
            catch (Exception ex)
            {
                // Llevar al Log de Excepciones
                throw ex;
            }
        }


    }
}
