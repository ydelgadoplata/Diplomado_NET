﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace LogDutyLog
{
    public class FachadaUsers
    {
        private DutyLogDBEntities _contexto;

        public FachadaUsers()
        {
            _contexto = new DutyLogDBEntities();
        }

        public int IngresarUsuario(Users users)
        {
            if (String.IsNullOrEmpty(users.Correo))
            {
                throw new Exception("Debe ingresar un correo");
            }

            _contexto.Users.Add(users);
            return _contexto.SaveChanges();
        }

        public Users RetornarUsuarios(string correo)
        {
            try
            {
                return _contexto.Users.Find(correo); //Solo busca en la BBDD por la key de la tabla
            }
            catch (Exception UsuarioNoEncontrado)
            {
                throw UsuarioNoEncontrado = new FormatException("Usuario no registrado");
            }
        }


        public bool ValidarUsuario(string correo, string pass)
        {
            try
            {
                Users validarusuario = RetornarUsuarios(correo);

                Users users = new Users();

                if (validarusuario.Correo == correo)
                {
                    if (validarusuario.Contrasena == pass)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                
            }
            catch (Exception UsuarioNoExiste)
            {

                throw UsuarioNoExiste = new FormatException("Usuario inválido o no registrado");
            }
        }




    }
}
