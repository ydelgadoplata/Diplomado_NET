﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    public class DAOCiudades
    {
        public List<Ciudad> RetornarCiudades()
        {
            using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString))
            {
                using (SqlCommand comando = new SqlCommand())
                {
                    comando.Connection = conexion;
                    comando.CommandText = "select A.Id as IdCiudad, A.NombreCiudad, B.Id as IdDepartamento, B.NombreDepartamento from Ciudad as A INNER JOIN Departamento as B on A.IdDepartamento = B.Id";

                    conexion.Open();

                    SqlDataReader sqlDataReader = comando.ExecuteReader();

                    List<Ciudad> ciudades = new List<Ciudad>();

                    while (sqlDataReader.Read())
                    {
                        Ciudad ciudad = new Ciudad
                        {
                            Id = Convert.ToInt32(sqlDataReader["IdCiudad"]),
                            NombreCiudad = sqlDataReader["NombreCiudad"].ToString(),
                            Departamento = new Departamento
                            {
                                Id = Convert.ToInt32(sqlDataReader["IdDepartamento"]),
                                NombreDepartamento = sqlDataReader["NombreDepartamento"].ToString()
                            }
                        };

                        ciudades.Add(ciudad);
                    }

                    sqlDataReader.Close();
                    conexion.Close();

                    return ciudades;
                }
            }
        }

        public Ciudad RetornarCiudad(int idCiudad)
        {
            using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString))
            {
                using (SqlCommand comando = new SqlCommand())
                {
                    comando.Connection = conexion;
                    comando.CommandText = "select A.Id as IdCiudad, A.NombreCiudad, B.Id as IdDepartamento, B.NombreDepartamento from Ciudad as A INNER JOIN Departamento as B on A.IdDepartamento = B.Id WHERE A.Id = " + idCiudad;

                    conexion.Open();

                    SqlDataReader sqlDataReader = comando.ExecuteReader();
                    Ciudad ciudad = null;

                    if (sqlDataReader.Read())
                    {
                        ciudad = new Ciudad();

                        ciudad.Id = Convert.ToInt32(sqlDataReader["IdCiudad"]);
                        ciudad.NombreCiudad = sqlDataReader["NombreCiudad"].ToString();
                        ciudad.Departamento = new Departamento

                        {
                          Id = Convert.ToInt32(sqlDataReader["IdDepartamento"]),
                          NombreDepartamento = sqlDataReader["NombreDepartamento"].ToString()  
                        };
                    }

                    sqlDataReader.Close();
                    conexion.Close();

                    return ciudad;
                }
            }
        }

        public void ActualizarCiudad(Ciudad ciudad)
        {
            using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString))
            {
                using (SqlCommand comando = new SqlCommand())
                {
                    comando.Connection = conexion;
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.CommandText = "ActualizarCiudad";

                    SqlParameter parametroId = new SqlParameter("@Id", SqlDbType.Int);
                    SqlParameter parametroNombre = new SqlParameter("@NombreCiudad", SqlDbType.NVarChar, 1000);
                    SqlParameter parametroIdDepartamento = new SqlParameter("@IdDepartamento", SqlDbType.Int);

                    parametroId.Value = ciudad.Id;
                    parametroNombre.Value = ciudad.NombreCiudad;
                    parametroIdDepartamento.Value = ciudad.Departamento.Id;

                    comando.Parameters.Add(parametroId);
                    comando.Parameters.Add(parametroNombre);
                    comando.Parameters.Add(parametroIdDepartamento);

                    conexion.Open();
                    comando.ExecuteNonQuery();
                    conexion.Close();
                }
            }

        }

        public void EliminarCiudad(int idCiudad)
        {
            using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString))
            {
                using (SqlCommand comando = new SqlCommand())
                {
                    comando.Connection = conexion;
                    //comando.CommandType = CommandType.StoredProcedure;
                    //comando.CommandText = "EliminarCiudad";
                    comando.CommandText = "DELETE FROM Ciudad WHERE Id = " + idCiudad;

                    conexion.Open();
                    comando.ExecuteNonQuery();
                    conexion.Close();
                }
            }

        }
    }

    
}
