﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Cursos
{
    public partial class FormDepartamentos : Form
    {
        public FormDepartamentos()
        {
            InitializeComponent();
        }

        private void departamentoBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.departamentoBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dBCursosDataSet);

        }

        private void FormDepartamentos_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dBCursosDataSet.Departamento' table. You can move, or remove it, as needed.
            this.departamentoTableAdapter.Fill(this.dBCursosDataSet.Departamento);

        }
    }
}
