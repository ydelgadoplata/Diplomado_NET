﻿namespace WF_Cursos
{
    partial class FormSerializar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnJson = new System.Windows.Forms.Button();
            this.btnXml = new System.Windows.Forms.Button();
            this.txtIdCiudad = new System.Windows.Forms.TextBox();
            this.txtResultado = new System.Windows.Forms.TextBox();
            this.btnObjeto = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnJson
            // 
            this.btnJson.Location = new System.Drawing.Point(165, 49);
            this.btnJson.Name = "btnJson";
            this.btnJson.Size = new System.Drawing.Size(94, 23);
            this.btnJson.TabIndex = 0;
            this.btnJson.Text = "Generar JSON";
            this.btnJson.UseVisualStyleBackColor = true;
            this.btnJson.Click += new System.EventHandler(this.btnJson_Click);
            // 
            // btnXml
            // 
            this.btnXml.Location = new System.Drawing.Point(33, 49);
            this.btnXml.Name = "btnXml";
            this.btnXml.Size = new System.Drawing.Size(94, 23);
            this.btnXml.TabIndex = 1;
            this.btnXml.Text = "Generar XML";
            this.btnXml.UseVisualStyleBackColor = true;
            this.btnXml.Click += new System.EventHandler(this.btnXml_Click);
            // 
            // txtIdCiudad
            // 
            this.txtIdCiudad.Location = new System.Drawing.Point(91, 12);
            this.txtIdCiudad.Name = "txtIdCiudad";
            this.txtIdCiudad.Size = new System.Drawing.Size(119, 20);
            this.txtIdCiudad.TabIndex = 2;
            // 
            // txtResultado
            // 
            this.txtResultado.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtResultado.Location = new System.Drawing.Point(0, 153);
            this.txtResultado.Multiline = true;
            this.txtResultado.Name = "txtResultado";
            this.txtResultado.Size = new System.Drawing.Size(292, 139);
            this.txtResultado.TabIndex = 3;
            // 
            // btnObjeto
            // 
            this.btnObjeto.Location = new System.Drawing.Point(103, 94);
            this.btnObjeto.Name = "btnObjeto";
            this.btnObjeto.Size = new System.Drawing.Size(94, 23);
            this.btnObjeto.TabIndex = 1;
            this.btnObjeto.Text = "Generar Objeto";
            this.btnObjeto.UseVisualStyleBackColor = true;
            this.btnObjeto.Click += new System.EventHandler(this.btnObjeto_Click);
            // 
            // FormSerializar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 292);
            this.Controls.Add(this.txtResultado);
            this.Controls.Add(this.txtIdCiudad);
            this.Controls.Add(this.btnObjeto);
            this.Controls.Add(this.btnXml);
            this.Controls.Add(this.btnJson);
            this.Name = "FormSerializar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormSerializar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnJson;
        private System.Windows.Forms.Button btnXml;
        private System.Windows.Forms.TextBox txtIdCiudad;
        private System.Windows.Forms.TextBox txtResultado;
        private System.Windows.Forms.Button btnObjeto;
    }
}