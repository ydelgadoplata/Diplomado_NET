﻿using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Cursos
{
    public partial class FormCiudades : Form
    {
        private FachadaMaestras _fachadaMaestras;

        public FormCiudades()
        {
            InitializeComponent();
            _fachadaMaestras = new FachadaMaestras();
            
        }

        private void FormCiudades_Load(object sender, EventArgs e)
        {
            dgvCiudades.AutoGenerateColumns = false;
            dgvCiudades.DataSource = _fachadaMaestras.RetornarCiudades();
        }

        private void dgvCiudades_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridView dataGridView = sender as DataGridView;

                if (dataGridView.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >=0)
                {
                    if (e.ColumnIndex == 3)
                    {
                        //TODO: Editar
                        //FormCiudad formCiudad = new FormCiudad();
                        //formCiudad.Show();
                        int idCiudad = Convert.ToInt32(dataGridView.Rows[e.RowIndex].Cells["Id"].Value);
                        FormCiudad formCiudad = new FormCiudad(idCiudad);
                        formCiudad.ShowDialog();
                        dgvCiudades.DataSource = _fachadaMaestras.RetornarCiudades();
                    }
                    else if (e.ColumnIndex == 4)
                    {
                        DialogResult dialogResult = MessageBox.Show("Desea eliminar el registro?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

                        if (dialogResult == DialogResult.Yes)
                        {
                            // TODO: Eliminar
                            int idCiudad = Convert.ToInt32(dataGridView.Rows[e.RowIndex].Cells["Id"].Value);
                            _fachadaMaestras.ElminarCiudad(idCiudad);
                            dgvCiudades.DataSource = _fachadaMaestras.RetornarCiudades();
                            MessageBox.Show("Registro Eliminado");
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
