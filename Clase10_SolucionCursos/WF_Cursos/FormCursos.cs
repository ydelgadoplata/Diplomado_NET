﻿using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Cursos
{
    public partial class FormCursos : Form
    {
        public FormCursos()
        {
            InitializeComponent();
        }

        private void FormCursos_Load(object sender, EventArgs e)
        {
            FachadaMaestras fachadaMaestras = new FachadaMaestras();

            DataSet dataSet = fachadaMaestras.RetornarDataSetCursos();

            dgvCursos.DataSource = dataSet.Tables[0];
        }
    }
}
