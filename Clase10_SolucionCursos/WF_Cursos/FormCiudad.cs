﻿using Entidades;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Cursos
{
    public partial class FormCiudad : Form
    {
        FachadaMaestras _fachadaMaestras;
        private int _idCiudad;

        public FormCiudad()
        {
            InitializeComponent();
            _fachadaMaestras = new FachadaMaestras();
        }

        public FormCiudad(int idCiudad)
        {
            InitializeComponent();

            _idCiudad = idCiudad;
            txtId.Text = Convert.ToString(_idCiudad);
            txtId.Enabled = false;

            _fachadaMaestras = new FachadaMaestras();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            

            try
            {
                Ciudad ciudad = new Ciudad
                {
                    Id = Convert.ToInt32(txtId.Text),
                    NombreCiudad = txtNombre.Text,
                    Departamento = new Departamento()
                    {
                        Id = Convert.ToInt32(cmbDepartamento.SelectedValue)
                    }
                };

                _fachadaMaestras.ActualizarCiudad(ciudad);

                MessageBox.Show("Ciudad actualizada exitosamente");
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void FormCiudad_Load(object sender, EventArgs e)
        {
            try
            {
                cmbDepartamento.DataSource = _fachadaMaestras.RetornarDepartamentos();
                cmbDepartamento.DisplayMember = "NombreDepartamento";
                cmbDepartamento.ValueMember = "Id";

                Ciudad ciudad = _fachadaMaestras.RetornarCiudad(_idCiudad);

                if (ciudad != null)
                {
                    txtNombre.Text = ciudad.NombreCiudad;
                    cmbDepartamento.SelectedValue = ciudad.Departamento.Id;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
