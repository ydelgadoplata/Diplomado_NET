﻿using Entidades;
using LogicaNegocio;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace WF_Cursos
{
    public partial class FormSerializar : Form
    {
        FachadaMaestras _fachadaMaestras;



        public FormSerializar()
        {
            InitializeComponent();
            _fachadaMaestras = new FachadaMaestras();
        }

        private void btnXml_Click(object sender, EventArgs e)
        {
            try
            {
                Ciudad ciudad = _fachadaMaestras.RetornarCiudad(Convert.ToInt32(txtIdCiudad.Text));

                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Ciudad));
                StringWriter stringWriter = new StringWriter();
                xmlSerializer.Serialize(stringWriter, ciudad);

                txtResultado.Text = stringWriter.ToString();
            }
            catch (Exception ex)
            {

                ;
            }
        }

        private void btnJson_Click(object sender, EventArgs e)
        {
            try
            {
                Ciudad ciudad = _fachadaMaestras.RetornarCiudad(Convert.ToInt32(txtIdCiudad.Text));
                txtResultado.Text = JsonConvert.SerializeObject(ciudad);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnObjeto_Click(object sender, EventArgs e)
        {
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Ciudad));
                StringReader stringReader = new StringReader(txtResultado.Text);
                XmlTextReader xmlTextReader = new XmlTextReader(stringReader);

                Ciudad ciudad = xmlSerializer.Deserialize(xmlTextReader) as Ciudad;

                MessageBox.Show(ciudad.NombreCiudad, ciudad.NombreDepartamento);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
