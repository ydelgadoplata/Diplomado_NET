﻿using Entidades;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Cursos
{
    class Program
    {
        static void Main(string[] args)
        {
            FachadaMaestras fachadaMaestras = new FachadaMaestras();
            try
            {
                foreach (Departamento departamento in fachadaMaestras.RetornarDepartamentos())
                {
                    Console.WriteLine($"ID: {departamento.Id} - Nombre: {departamento.NombreDepartamento}");
                }

                Console.WriteLine("Ciudades");

                foreach (Ciudad ciudad in fachadaMaestras.RetornarCiudades())
                {
                    Console.WriteLine($"IdCiu: {ciudad.Id} - Ciudad: {ciudad.NombreCiudad} - IdDep: {ciudad.Departamento.Id} - Dpto: {ciudad.Departamento.NombreDepartamento}");
                }

                Console.WriteLine("Modificacion Ciudad");


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                Console.ReadKey();
            }

        }
    }
}
