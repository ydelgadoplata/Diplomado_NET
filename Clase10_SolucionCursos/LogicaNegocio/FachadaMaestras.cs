﻿using AccesoDatos;
using Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{
    public class FachadaMaestras
    {
        private DAODepartamento _daoDepartamento;
        private DAOCiudades _daoCiudades;
        private DAOCurso _daoCurso;

        public FachadaMaestras()
        {
            _daoDepartamento = new DAODepartamento();
            _daoCiudades = new DAOCiudades();
            _daoCurso = new DAOCurso();
        }

        #region [ Funciones Departamentos ]
        public List<Departamento> RetornarDepartamentos()
        {
            return _daoDepartamento.RetornarDepartamentos();
        } 
        #endregion

        public List<Ciudad> RetornarCiudades()
        {
            return _daoCiudades.RetornarCiudades();
        }

        public void ActualizarCiudad(Ciudad ciudad)
        {
            _daoCiudades.ActualizarCiudad(ciudad);
        }

        public void ElminarCiudad(int idCiudad)
        {
            _daoCiudades.EliminarCiudad(idCiudad);
        }

        public Ciudad RetornarCiudad(int idCiudad)
        {
            Ciudad ciudad = _daoCiudades.RetornarCiudad(idCiudad);

            if (ciudad == null)

            {
                throw new Exception("La ciudad no existe!");
            }

            return ciudad;
        }

        public DataSet RetornarDataSetCursos()
        {
            return _daoCurso.RetornarDataSetCurso();
        }

    }
}
