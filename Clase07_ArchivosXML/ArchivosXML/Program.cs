﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ArchivosXML
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(@"C:\Users\USUARIO\Source\Repos\Diplomado_NET_2018\Clase07_ArchivosXML\ArchivosXML\Empleados.xml");

            #region [ Empleados por pais ]
            XmlNodeList empleadosPorPais = xmlDocument.SelectNodes("/Empleados/Empleado[Pais='COL']");

            foreach (XmlNode nodo in empleadosPorPais)
            {
                Console.WriteLine($"Nombre: {nodo["Nombre"].InnerText} de {nodo.Attributes["Pais"].Value}");
            } 
            #endregion

            Console.WriteLine(Environment.NewLine);

            #region [ Empleados por Ciudad ]
            XmlNodeList empleadosPorCiudad = xmlDocument.SelectNodes("/Empleados/Empleado[@Ciudad='ENV']");

            foreach (XmlNode nodo in empleadosPorCiudad)
            {
                Console.WriteLine($"Nombre: {nodo["Nombre"].InnerText} de {nodo.Attributes["Ciudad"].Value}");
            }
            #endregion

            Console.WriteLine(Environment.NewLine);

            #region [ Empleados por Genero ]
            XmlNodeList empleadosPorGenero = xmlDocument.SelectNodes("/Empleados/Empleado[@Genero='M']");

            foreach (XmlNode nodo in empleadosPorGenero)
            {
                Console.WriteLine($"Nombre: {nodo["Nombre"].InnerText} Género: {nodo.Attributes["Genero"].Value}");
            }
            #endregion

            Console.WriteLine(Environment.NewLine);


            XmlElement empleado = xmlDocument.CreateElement("Empleado");


            //Atributos empleado
            XmlAttribute id = xmlDocument.CreateAttribute("Id"); id.Value = "5";

            XmlAttribute ciudad = xmlDocument.CreateAttribute("Ciudad"); id.Value = "ENV";

            XmlAttribute gender = xmlDocument.CreateAttribute("Genero"); gender.Value = "F";

            empleado.Attributes.Append(id);
            empleado.Attributes.Append(ciudad);
            empleado.Attributes.Append(gender);

            //Elementos Empleado
            XmlElement nombre = xmlDocument.CreateElement("Nombre"); nombre.InnerText = "Maria";
            XmlElement pais = xmlDocument.CreateElement("Pais"); pais.InnerText = "COL";

            empleado.AppendChild(nombre);
            empleado.AppendChild(pais);

            xmlDocument.DocumentElement.AppendChild(empleado);
            xmlDocument.Save(@"C:\Users\USUARIO\Source\Repos\Diplomado_NET_2018\Clase07_ArchivosXML\ArchivosXML\EmpleadosNEW.xml");
        }
    }
}
