﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Controles
{
    public partial class FormOtrosControles : Form
    {
        public FormOtrosControles()
        {
            InitializeComponent();
        }

        private void FormOtrosControles_Load(object sender, EventArgs e)
        {
            btnCentro.Text = "Centro";
            btnDerecho.Text = "Derecho";
            btnEjecutar.Text = "Ejecutar";
            btnIzquierdo.Text = "Izquierdo";
            btnSuperior.Text = "Superior";

            pbBarraProgreso.Maximum = 200;

            toolTip.SetToolTip(btnEjecutar, "Espera a que cargue");
        }

        private void btnEjecutar_Click(object sender, EventArgs e)
        {
            btnEjecutar.Enabled = false;

            for (int i = 0; i < 100; i++)
            {
                Thread.Sleep(100);
                pbBarraProgreso.Value = i;
            }

            pbBarraProgreso.Value = 200;
            btnEjecutar.Enabled = true;
        }

        private void btnValidar_Click(object sender, EventArgs e)
        {

            int edad = 0;

            bool esNum = int.TryParse(txtEdad.Text, out edad);

            if (!esNum)
            {
                errorProvider1.SetError(txtEdad, "Solo numeros");
                return;
            }


            if (txtEdad.Text.Length == 0)
            {
                errorProvider1.SetError(txtEdad, "Debe colocar valor");
                return;
            }

            errorProvider1.SetError(txtEdad, "");
            MessageBox.Show("Edad Correcta");
        }
    }
}
