﻿namespace WF_Controles
{
    partial class FormOtrosControles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnIzquierdo = new System.Windows.Forms.Button();
            this.btnCentro = new System.Windows.Forms.Button();
            this.btnDerecho = new System.Windows.Forms.Button();
            this.btnSuperior = new System.Windows.Forms.Button();
            this.pbBarraProgreso = new System.Windows.Forms.ProgressBar();
            this.btnEjecutar = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.txtEdad = new System.Windows.Forms.TextBox();
            this.btnValidar = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnIzquierdo
            // 
            this.btnIzquierdo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnIzquierdo.Location = new System.Drawing.Point(30, 407);
            this.btnIzquierdo.Name = "btnIzquierdo";
            this.btnIzquierdo.Size = new System.Drawing.Size(75, 23);
            this.btnIzquierdo.TabIndex = 0;
            this.btnIzquierdo.Text = "button1";
            this.btnIzquierdo.UseVisualStyleBackColor = true;
            // 
            // btnCentro
            // 
            this.btnCentro.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCentro.Location = new System.Drawing.Point(123, 407);
            this.btnCentro.Name = "btnCentro";
            this.btnCentro.Size = new System.Drawing.Size(75, 23);
            this.btnCentro.TabIndex = 1;
            this.btnCentro.Text = "button2";
            this.btnCentro.UseVisualStyleBackColor = true;
            // 
            // btnDerecho
            // 
            this.btnDerecho.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDerecho.Location = new System.Drawing.Point(215, 407);
            this.btnDerecho.Name = "btnDerecho";
            this.btnDerecho.Size = new System.Drawing.Size(75, 23);
            this.btnDerecho.TabIndex = 2;
            this.btnDerecho.Text = "button3";
            this.btnDerecho.UseVisualStyleBackColor = true;
            // 
            // btnSuperior
            // 
            this.btnSuperior.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSuperior.Location = new System.Drawing.Point(0, 0);
            this.btnSuperior.Name = "btnSuperior";
            this.btnSuperior.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnSuperior.Size = new System.Drawing.Size(330, 23);
            this.btnSuperior.TabIndex = 3;
            this.btnSuperior.Text = "button4";
            this.btnSuperior.UseVisualStyleBackColor = true;
            // 
            // pbBarraProgreso
            // 
            this.pbBarraProgreso.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pbBarraProgreso.Location = new System.Drawing.Point(-29, 37);
            this.pbBarraProgreso.Name = "pbBarraProgreso";
            this.pbBarraProgreso.Size = new System.Drawing.Size(389, 23);
            this.pbBarraProgreso.TabIndex = 4;
            // 
            // btnEjecutar
            // 
            this.btnEjecutar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnEjecutar.Location = new System.Drawing.Point(114, 66);
            this.btnEjecutar.Name = "btnEjecutar";
            this.btnEjecutar.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnEjecutar.Size = new System.Drawing.Size(101, 23);
            this.btnEjecutar.TabIndex = 5;
            this.btnEjecutar.Text = "button4";
            this.btnEjecutar.UseVisualStyleBackColor = true;
            this.btnEjecutar.Click += new System.EventHandler(this.btnEjecutar_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(146, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "label1";
            // 
            // txtEdad
            // 
            this.txtEdad.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.txtEdad.Location = new System.Drawing.Point(114, 111);
            this.txtEdad.Name = "txtEdad";
            this.txtEdad.Size = new System.Drawing.Size(100, 20);
            this.txtEdad.TabIndex = 7;
            // 
            // btnValidar
            // 
            this.btnValidar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnValidar.Location = new System.Drawing.Point(123, 137);
            this.btnValidar.Name = "btnValidar";
            this.btnValidar.Size = new System.Drawing.Size(75, 23);
            this.btnValidar.TabIndex = 8;
            this.btnValidar.Text = "Validar";
            this.btnValidar.UseVisualStyleBackColor = true;
            this.btnValidar.Click += new System.EventHandler(this.btnValidar_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // FormOtrosControles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(330, 446);
            this.Controls.Add(this.btnValidar);
            this.Controls.Add(this.txtEdad);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnEjecutar);
            this.Controls.Add(this.pbBarraProgreso);
            this.Controls.Add(this.btnSuperior);
            this.Controls.Add(this.btnDerecho);
            this.Controls.Add(this.btnCentro);
            this.Controls.Add(this.btnIzquierdo);
            this.Name = "FormOtrosControles";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormOtrosControles";
            this.Load += new System.EventHandler(this.FormOtrosControles_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnIzquierdo;
        private System.Windows.Forms.Button btnCentro;
        private System.Windows.Forms.Button btnDerecho;
        private System.Windows.Forms.Button btnSuperior;
        private System.Windows.Forms.ProgressBar pbBarraProgreso;
        private System.Windows.Forms.Button btnEjecutar;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtEdad;
        private System.Windows.Forms.Button btnValidar;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}