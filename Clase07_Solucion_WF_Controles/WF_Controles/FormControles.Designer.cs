﻿namespace WF_Controles
{
    partial class FormControles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormControles));
            this.lblNombre = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.mtxtFecha = new System.Windows.Forms.MaskedTextBox();
            this.rtxtContenido = new System.Windows.Forms.RichTextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.cnEstadoCivil = new System.Windows.Forms.GroupBox();
            this.chklstLenguajes = new System.Windows.Forms.CheckedListBox();
            this.btnObtenerInfo = new System.Windows.Forms.Button();
            this.rdoMasculino = new System.Windows.Forms.RadioButton();
            this.rdoFemenino = new System.Windows.Forms.RadioButton();
            this.rdoSoltero = new System.Windows.Forms.RadioButton();
            this.rdoCasado = new System.Windows.Forms.RadioButton();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.cnEstadoCivil.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(30, 6);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(47, 13);
            this.lblNombre.TabIndex = 0;
            this.lblNombre.Text = "Nombre:";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(30, 24);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(215, 20);
            this.txtNombre.TabIndex = 1;
            // 
            // mtxtFecha
            // 
            this.mtxtFecha.Location = new System.Drawing.Point(30, 49);
            this.mtxtFecha.Mask = "00/00/0000";
            this.mtxtFecha.Name = "mtxtFecha";
            this.mtxtFecha.Size = new System.Drawing.Size(215, 20);
            this.mtxtFecha.TabIndex = 2;
            this.mtxtFecha.ValidatingType = typeof(System.DateTime);
            // 
            // rtxtContenido
            // 
            this.rtxtContenido.Location = new System.Drawing.Point(30, 74);
            this.rtxtContenido.Name = "rtxtContenido";
            this.rtxtContenido.Size = new System.Drawing.Size(215, 96);
            this.rtxtContenido.TabIndex = 3;
            this.rtxtContenido.Text = "";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Fútbol",
            "Cine",
            "Lectura",
            "Caminatas"});
            this.comboBox1.Location = new System.Drawing.Point(30, 175);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(215, 21);
            this.comboBox1.TabIndex = 4;
            // 
            // cnEstadoCivil
            // 
            this.cnEstadoCivil.Controls.Add(this.rdoSoltero);
            this.cnEstadoCivil.Controls.Add(this.rdoCasado);
            this.cnEstadoCivil.Location = new System.Drawing.Point(30, 266);
            this.cnEstadoCivil.Name = "cnEstadoCivil";
            this.cnEstadoCivil.Size = new System.Drawing.Size(215, 61);
            this.cnEstadoCivil.TabIndex = 5;
            this.cnEstadoCivil.TabStop = false;
            this.cnEstadoCivil.Text = "Estado Civil";
            // 
            // chklstLenguajes
            // 
            this.chklstLenguajes.FormattingEnabled = true;
            this.chklstLenguajes.Items.AddRange(new object[] {
            "C#",
            "HTML",
            "JavaScript",
            "C++"});
            this.chklstLenguajes.Location = new System.Drawing.Point(30, 371);
            this.chklstLenguajes.Name = "chklstLenguajes";
            this.chklstLenguajes.Size = new System.Drawing.Size(215, 94);
            this.chklstLenguajes.TabIndex = 6;
            // 
            // btnObtenerInfo
            // 
            this.btnObtenerInfo.Location = new System.Drawing.Point(57, 482);
            this.btnObtenerInfo.Name = "btnObtenerInfo";
            this.btnObtenerInfo.Size = new System.Drawing.Size(157, 32);
            this.btnObtenerInfo.TabIndex = 7;
            this.btnObtenerInfo.Text = "Obtener Información";
            this.btnObtenerInfo.UseVisualStyleBackColor = true;
            this.btnObtenerInfo.Click += new System.EventHandler(this.btnObtenerInfo_Click);
            // 
            // rdoMasculino
            // 
            this.rdoMasculino.AutoSize = true;
            this.rdoMasculino.Location = new System.Drawing.Point(174, 217);
            this.rdoMasculino.Name = "rdoMasculino";
            this.rdoMasculino.Size = new System.Drawing.Size(34, 17);
            this.rdoMasculino.TabIndex = 9;
            this.rdoMasculino.TabStop = true;
            this.rdoMasculino.Text = "M";
            this.rdoMasculino.UseVisualStyleBackColor = true;
            // 
            // rdoFemenino
            // 
            this.rdoFemenino.AutoSize = true;
            this.rdoFemenino.Location = new System.Drawing.Point(57, 218);
            this.rdoFemenino.Name = "rdoFemenino";
            this.rdoFemenino.Size = new System.Drawing.Size(31, 17);
            this.rdoFemenino.TabIndex = 8;
            this.rdoFemenino.TabStop = true;
            this.rdoFemenino.Text = "F";
            this.rdoFemenino.UseVisualStyleBackColor = true;
            // 
            // rdoSoltero
            // 
            this.rdoSoltero.AutoSize = true;
            this.rdoSoltero.Location = new System.Drawing.Point(124, 21);
            this.rdoSoltero.Name = "rdoSoltero";
            this.rdoSoltero.Size = new System.Drawing.Size(58, 17);
            this.rdoSoltero.TabIndex = 11;
            this.rdoSoltero.TabStop = true;
            this.rdoSoltero.Text = "Soltero";
            this.rdoSoltero.UseVisualStyleBackColor = true;
            // 
            // rdoCasado
            // 
            this.rdoCasado.AutoSize = true;
            this.rdoCasado.Location = new System.Drawing.Point(32, 22);
            this.rdoCasado.Name = "rdoCasado";
            this.rdoCasado.Size = new System.Drawing.Size(61, 17);
            this.rdoCasado.TabIndex = 10;
            this.rdoCasado.TabStop = true;
            this.rdoCasado.Text = "Casado";
            this.rdoCasado.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(30, 333);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 10;
            // 
            // FormControles
            // 
            this.AccessibleDescription = "VENTANA DE ARRANQUE DE WF";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(278, 530);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.rdoMasculino);
            this.Controls.Add(this.rdoFemenino);
            this.Controls.Add(this.btnObtenerInfo);
            this.Controls.Add(this.chklstLenguajes);
            this.Controls.Add(this.cnEstadoCivil);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.rtxtContenido);
            this.Controls.Add(this.mtxtFecha);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.lblNombre);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormControles";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VENTANA DE PRUEBA";
            this.cnEstadoCivil.ResumeLayout(false);
            this.cnEstadoCivil.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.MaskedTextBox mtxtFecha;
        private System.Windows.Forms.RichTextBox rtxtContenido;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.GroupBox cnEstadoCivil;
        private System.Windows.Forms.CheckedListBox chklstLenguajes;
        private System.Windows.Forms.Button btnObtenerInfo;
        private System.Windows.Forms.RadioButton rdoMasculino;
        private System.Windows.Forms.RadioButton rdoFemenino;
        private System.Windows.Forms.RadioButton rdoSoltero;
        private System.Windows.Forms.RadioButton rdoCasado;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}

