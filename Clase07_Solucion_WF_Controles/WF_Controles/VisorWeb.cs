﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace WF_Controles
{
    public partial class VisorWeb : Form
    {
        public VisorWeb()
        {
            InitializeComponent();
        }

        private void btnIr_Click(object sender, EventArgs e)
        {
            string urlaverificar = textBox1.Text;

            Regex regex = new Regex(@"(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?");

            Match match = regex.Match(urlaverificar);

            if (match.Success)
            {
                string urldigitada = textBox1.Text;
                webBrowser.Navigate(textBox1.Text);
                MessageBox.Show (@"La URL digitada es: {0}, {textBox1.Text}");
            }
            else
            {
                MessageBox.Show("La URL digitada no es correcta!!!");
            }
            
        }
    }
}
