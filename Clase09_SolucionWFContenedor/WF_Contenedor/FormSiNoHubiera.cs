﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Contenedor
{
    public partial class FormSiNoHubiera : Form
    {
        public FormSiNoHubiera()
        {
            InitializeComponent();
        }

        private void cmbPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbDepartamento.Text = "";
            cmbDepartamento.Items.Clear();
            if (cmbPais.Text == "Colombia")
            {
                cmbDepartamento.Items.Add("Antioquia");
                cmbDepartamento.Items.Add("Valle");
                cmbDepartamento.Items.Add("Santander");
            }
            else if(cmbPais.Text == "Ecuador")
            {
                cmbDepartamento.Items.Add("Pichincha");
                cmbDepartamento.Items.Add("Pichincha2");
                cmbDepartamento.Items.Add("Pichincha3");
            }
            else if(cmbPais.Text == "Brasil")
            {
                cmbDepartamento.Items.Add("Botafogo");
                cmbDepartamento.Items.Add("Rio de Janeiro");
            }
        }
    }
}
