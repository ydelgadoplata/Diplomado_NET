﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFormTabla.aspx.cs" Inherits="AplicacionWeb.WebFormTabla" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table border ="2">
                <tr>
                    <td>Dato1</td>
                    <td>Dato2</td>
                    <td>Dato3</td>
                </tr>
                <tr>
                    <td>Dato4</td>
                    <td>Dato5</td>
                    <td>Dato6</td>
                </tr>
                <tr>
                    <td>Dato7</td>
                    <td colspan="2">Dato8</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
