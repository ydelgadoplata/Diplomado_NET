﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AplicacionWeb
{
    public partial class WebFormControles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnObtenerInfo_Click(object sender, EventArgs e)
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(Calendario.SelectedDate.ToString("yyyy-MMM-dd") + "<br/>");

            stringBuilder.Append(chkTieneCarro.Checked ? "Tiene Carro!!!<br/>" : "Es pobre!!!<br/>");

            foreach (ListItem item in chklstLenguajes.Items)
            {
                if (item.Selected)
                {
                    stringBuilder.Append(item.Text + "<br/>");
                }
            }

            stringBuilder.Append(ddlCiudad.SelectedValue + "<br/>");

            stringBuilder.Append("Valor Oculto: " + hdfValorOculto.Value + "<br/>");

            foreach (ListItem item in lstPasatiempos.Items)
            {
                if (item.Selected)
                {
                    stringBuilder.Append(item.Text + "<br/>");
                }
            }

            stringBuilder.Append(rdoVerdadero.Checked ? "VERDADERO<br/>" : "FALSO<br/>");

            stringBuilder.Append(rdolstOpciones.SelectedValue + "<br/>");

            //string Fecha;
            //string LenguajeSel;
            //string TieneCar;

            //Fecha = Calendario.SelectedDate.ToString();
            //LenguajeSel = chklstLenguajes.SelectedItem.ToString();
            //if (chkTieneCarro.Checked)
            //{
            //    TieneCar = "Tiene Carro!";
            //}
            //else
            //{
            //    TieneCar = "No tiene Carro!";
            //}


            //lblValores.Text = Fecha + " " + TieneCar + LenguajeSel;

            lblValores.Text = stringBuilder.ToString();
        }
    }
}