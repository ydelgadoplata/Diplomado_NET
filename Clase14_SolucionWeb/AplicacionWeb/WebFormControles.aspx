﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFormControles.aspx.cs" Inherits="AplicacionWeb.WebFormControles" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table border="1">
                <tr>
                    <th>Nombre del control</th>
                    <th>Control</th>
                </tr>
                <tr>
                    <td>Calendario</td>
                    <td>
                        <asp:Calendar ID="Calendario" runat="server" BackColor="White" BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#003399" Height="200px" Width="220px">
                            <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                            <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                            <OtherMonthDayStyle ForeColor="#999999" />
                            <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                            <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                            <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                            <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                            <WeekendDayStyle BackColor="#99ffcc" />
                        </asp:Calendar>
                    </td>
                </tr>
                <tr>
                    <td>Checkbox</td>
                    <td>
                        <asp:CheckBox ID="chkTieneCarro" runat="server" Text="Tiene carro?" Checked="true" /></td>
                </tr>
                <tr>
                    <td>CheckboxList</td>
                    <td>
                        <asp:CheckBoxList ID="chklstLenguajes" runat="server">
                            <asp:ListItem>C#</asp:ListItem>
                            <asp:ListItem>HTML</asp:ListItem>
                            <asp:ListItem>JavaScript</asp:ListItem>
                            <asp:ListItem>Basic</asp:ListItem>
                        </asp:CheckBoxList></td>
                </tr>
                <tr>
                    <td>DropDownList</td>
                    <td>
                        <asp:DropDownList ID="ddlCiudad" runat="server">
                            <asp:ListItem>Medellín</asp:ListItem>
                            <asp:ListItem>Envigado</asp:ListItem>
                            <asp:ListItem>Itagüí</asp:ListItem>
                            <asp:ListItem>Bello</asp:ListItem>
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>HiddenField</td>
                    <td>
                        <asp:HiddenField ID="hdfValorOculto" runat="server" Value="100" />
                    </td>
                </tr>
                <tr>
                    <td>HyperLinURL</td>
                    <td>
                        <asp:HyperLink ID="hlURL" runat="server" NavigateUrl="http://www.google.com" Target="_blank">Google</asp:HyperLink></td>
                </tr>
                <tr>
                    <td>Imagen</td>
                    <td>
                        <asp:Image ImageUrl="~/Images/mt-03.png" Width="100" Height="150" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>ListBox</td>
                    <td>
                        <asp:ListBox runat="server" ID="lstPasatiempos" SelectionMode="Multiple" Rows="4">
                            <asp:ListItem>xBox</asp:ListItem>
                            <asp:ListItem>Dormir</asp:ListItem>
                            <asp:ListItem>Rodar</asp:ListItem>
                            <asp:ListItem>Comer</asp:ListItem>
                            <asp:ListItem>PaintBall</asp:ListItem>
                        </asp:ListBox>

                    </td>
                </tr>
                <tr>
                    <td>Label</td>
                    <td>
                        <asp:Label Text="Información" ID="lblInfo" runat="server" ToolTip="Ejemplo de label"/>
                    </td>
                </tr>
                <tr>
                    <td>RadioButton</td>
                    <td>
                        <asp:RadioButton Text="Verdadero" ID="rdoVerdadero" Checked="true" GroupName="radio" runat="server" />
                        <asp:RadioButton Text="Falso" ID="rdoFalso" GroupName="radio" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>RadioButtonList</td>
                    <td>
                        <asp:RadioButtonList ID="rdolstOpciones" ToolTip="Haga Click en su elección" runat="server">
                            <asp:ListItem Text="Opción 1" />
                            <asp:ListItem Text="Opción 2" />
                            <asp:ListItem Text="Opción 3" />
                            <asp:ListItem Text="Opción 4" Selected="True"/>
                            <asp:ListItem Text="Opción 5" />
                            <asp:ListItem Text="Opción 6" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <asp:Button Text="¡¡¡Click aquí!!!" ID="btnObtenerInfo" OnClick="btnObtenerInfo_Click" ToolTip="Haga Click aquí para obtener valores" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label runat="server" ID="lblValores"/>
                    </td>
                    <%--//Calendario: Fecha Seleccionada (tecla rapida comentar ctrl + k + c)
                    //CheckBox
                    //CheckboxList
                    //Hidden
                    //ListBox
                    //RadioButtonList--%>
                </tr>
                
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
