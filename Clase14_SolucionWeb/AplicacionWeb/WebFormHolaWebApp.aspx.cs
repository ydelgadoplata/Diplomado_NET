﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AplicacionWeb
{
    public partial class WebFormHolaWebApp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblSaludo.Text = DateTime.Now.ToLongDateString();
            
        }

        protected void btnSaludar_Click(object sender, EventArgs e)
        {
            lblSaludo.Text = "Bienvenido: " + txtNombre.Text;
        }
    }
}