﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Figuras
{
    public class Circulo : Figura
    {
        public Circulo(Color color, string nombreFigura) : base(color)
        {
            NombreFigura = nombreFigura;
        }

        public override void Pintar()
        {
            Console.WriteLine($"Pinta el círculo {NombreFigura} de color {ColorFigura}");
        }
    }
}
