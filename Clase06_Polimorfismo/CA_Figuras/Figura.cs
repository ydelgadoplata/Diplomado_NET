﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Figuras
{
    public class Figura
    {
        public Color ColorFigura { get; set; }

        public string NombreFigura { get; set; } 

        //Constructor que recibe solamente el color de la figura
        public Figura(Color color)
        {
            ColorFigura = color;
        }

        public virtual void Pintar()
        {
            Console.WriteLine($"Pinta una figura de color {ColorFigura}");
        }


    }
}
