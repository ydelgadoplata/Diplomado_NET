﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Figuras
{
    public class Linea : Figura
    {
        public Linea(Color color, string nombreFigura) : base(color)
        {
            NombreFigura = nombreFigura;
        }

        public override void Pintar()
        {
            Console.WriteLine($"Pinta la línea {NombreFigura} de color {ColorFigura}");
        }
    }
}
