﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Figuras
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Figura> figuras = new List<Figura>();
            figuras.Add(new Figura(Color.Azul));
            figuras.Add(new Linea(Color.Rojo, "Linea AA"));
            figuras.Add(new Circulo(Color.Amarillo, "Circulo vicioso"));

            foreach (Figura figura in figuras)
            {
                figura.Pintar();
            }

            Console.ReadLine();
        }
    }
}
