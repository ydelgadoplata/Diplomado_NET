﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BootstrapMaster.Master" AutoEventWireup="true" CodeBehind="WebFormGenerarError.aspx.cs" Inherits="WA_Bootstrap.Formularios.WebFormGenerarError" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mt-3">
        <div class="card">
            <div class="card-header bg-primary text-white">
                Generar Error
            </div>
            <div class="card-body">
                
                <div class="form-group row">
                    <div class="offset-md-2 col-md-4">
                        <asp:Button Text="Generar Error" runat="server" ID="btnError" CssClass="btn btn-primary btn-block" OnClick="btnError_Click" />
                    </div>
                  
                </div>
                <div class="form-group row">
                    <div class="offset-md-2 col-md-3">
                        <asp:Label Text="" runat="server" ID="lblInfo" CssClass="text-success" />
                        <//div>
                    </div>
            </div>
       </div>
  </div>
</asp:Content>
