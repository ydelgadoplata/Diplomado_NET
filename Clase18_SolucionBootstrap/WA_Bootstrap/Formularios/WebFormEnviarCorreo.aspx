﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BootstrapMaster.Master" AutoEventWireup="true" CodeBehind="WebFormEnviarCorreo.aspx.cs" Inherits="WA_Bootstrap.Formularios.WebFormEnviarCorreo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div class="mt-3">
        <div class="card">
            <div class="card-header bg-primary text-white">
                Enviar Correo
            </div>
            <div class="card-body">
                <asp:ValidationSummary runat="server" ID="vsResumen" DisplayMode="BulletList" ShowMessageBox="false" ShowSummary="true" CssClass="alert alert-danger"/>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtPara">Para:</label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtPara" Placeholder="example@example.com" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtAsunto">Asunto:</label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtAsunto" Placeholder="" />
                    </div>
                </div>
                 <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtContenido">Contenido:</label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" TextMode="MultiLine" CssClass="form-control" ID="txtContenido" Height="200" Placeholder="" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-md-2 col-md-4">
                        <asp:Button Text="Enviar" runat="server" ID="btnEnviar" CssClass="btn btn-primary btn-block" OnClick="btnEnviar_Click" />
                    </div>
                    <div class="offset-md-2 col-md-4">
                        <asp:Button Text="Limpiar" runat="server" ID="btnLimpiar" CssClass="btn btn-outline-danger btn-block" onclick="btnLimpiar_Click" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-md-2 col-md-3">
                        <asp:Label Text="" runat="server" ID="lblInfo" CssClass="text-success" />
                        <//div>
                    </div>
            </div>
       </div>
  </div>
</asp:Content>
