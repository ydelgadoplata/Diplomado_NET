﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WA_Bootstrap.Formularios
{
    public partial class WebFormSubirArchivo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCargar_Click(object sender, EventArgs e)
        {
            if (fuArchivo.HasFile)
            {
                try
                {
                    string nombreArchivo = Path.GetFileName(fuArchivo.FileName);

                    fuArchivo.SaveAs(Server.MapPath("~/Files/") + nombreArchivo);

                    lblInfo.CssClass = "text-success";
                    lblInfo.Text = "Archivo subido correctamente";
                }
                catch (Exception ex)
                {
                    lblInfo.CssClass = "text-danger";
                    lblInfo.Text = $"error: {ex.Message}";
                }
            }
        }
    }
}