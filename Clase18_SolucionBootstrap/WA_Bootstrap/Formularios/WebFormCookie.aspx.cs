﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WA_Bootstrap.Formularios
{
    public partial class WebFormCookie : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            HttpCookie httpCookie = new HttpCookie("Galleta");

            httpCookie.Value = txtValorCookie.Text;
            httpCookie.Expires = DateTime.Now.AddSeconds(60);

            Response.Cookies.Add(httpCookie);
            lblInfo.Text = "La cookie se ha creado exitosamente";
        }

        protected void btnLeer_Click(object sender, EventArgs e)
        {
            HttpCookie httpCookie = Request.Cookies["Galleta"];

            if (httpCookie != null)
            {
                txtValorCookie.Text = httpCookie.Value;
            }

            else
            {
                lblInfo.Text = "Cookie no existe!";
            }

        }
    }
}