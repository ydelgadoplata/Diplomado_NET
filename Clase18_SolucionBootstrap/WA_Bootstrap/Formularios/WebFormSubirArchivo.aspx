﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BootstrapMaster.Master" AutoEventWireup="true" CodeBehind="WebFormSubirArchivo.aspx.cs" Inherits="WA_Bootstrap.Formularios.WebFormSubirArchivo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mt-3">
        <div class="card">
            <div class="card-header bg-primary text-white">
                Subir Archivo
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Archivo:</label>
                    <div class="col-md-10">
                        <asp:FileUpload runat="server" ID="fuArchivo" CssClass="form-control" />
                    </div>
                </div>
               </div>
                <div class="form-group row">
                    <div class="offset-md-2 col-md-3">
                        <asp:Button Text="Cargar" runat="server" ID="btnCargar" CssClass="btn btn-primary btn-block" OnClick="btnCargar_Click" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-md-2 col-md-3">
                        <asp:Label Text="" runat="server" ID="lblInfo" CssClass="text-success" />
                        <//div>
                    </div>
            </div>
        </div>
</asp:Content>
