﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BootstrapMaster.Master" AutoEventWireup="true" CodeBehind="WebFormIsPostBack.aspx.cs" Inherits="WA_Bootstrap.Formularios.WebFormIsPostBack" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mt-3">
        <div class="card">
            <div class="card-header bg-primary text-white">
                PostBack
            </div>
             <div class="card-body">
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">País:</label>
                    <div class="col-md-10">
                        <asp:DropDownList runat="server" ID="ddlPais" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlPais_SelectedIndexChanged"> 
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Departamento:</label>
                    <div class="col-md-10">
                        <asp:DropDownList runat="server" ID="ddlDepartamento" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged"> 
                        </asp:DropDownList>
                    </div>
                </div>
                 <div class="form-group row">
                    <label class="col-md-2 col-form-label">Ciudad:</label>
                    <div class="col-md-10">
                        <asp:DropDownList runat="server" ID="ddlCiudad" CssClass="form-control" AutoPostBack="true"> 
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-md-2 col-md-4">
                        <asp:Button Text="Hacer petición" runat="server" ID="btnHacerPeticion" CssClass="btn btn-primary btn-block" OnClick="btnHacerPeticion_Click" />
                    </div>
                    <div class="offset-md-2 col-md-4">
                        <asp:Button Text="Limpiar" runat="server" ID="btnLimpiar" CssClass="btn btn-outline-danger btn-block" OnClick="btnLimpiar_Click" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-md-2 col-md-3">
                        <asp:Label Text="" runat="server" ID="lblInfo" CssClass="text-success" />
                        <//div>
                    </div>
            </div>
       </div>
  </div>
</asp:Content>
