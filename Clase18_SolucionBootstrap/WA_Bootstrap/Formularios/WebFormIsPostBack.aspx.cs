﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WA_Bootstrap.Formularios
{
    public partial class WebFormIsPostBack : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ddlPais.Items.Add(new ListItem("-- Seleccione --"));
                ddlPais.Items.Add(new ListItem("Colombia"));
                ddlPais.Items.Add(new ListItem("Ecuador"));
                ddlPais.Items.Add(new ListItem("Perú"));
                ddlPais.Items.Add(new ListItem("Rep. Dominicana"));
            }
        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            //ddlPais.Items.Clear();
        }

        protected void btnHacerPeticion_Click(object sender, EventArgs e)
        {
            lblInfo.Text = @"El país seleccionado es: " + ddlPais.SelectedValue + ", el departamento es: " + ddlDepartamento.SelectedValue + " y la ciudad es: " + ddlCiudad.SelectedValue;
        }

        protected void ddlPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack)
            //{
            ddlDepartamento.Items.Clear();
            ddlCiudad.Items.Clear();

            switch (ddlPais.SelectedValue)
            {
                case "Colombia":
                    //ddlDepartamento.Items.Clear();
                    ddlDepartamento.Items.Add("-- Seleccione --");
                    ddlDepartamento.Items.Add("Antioquia");
                    ddlDepartamento.Items.Add("Santander");
                    break;
                case "Ecuador":
                    //ddlDepartamento.Items.Clear();
                    ddlDepartamento.Items.Add("-- Seleccione --");
                    ddlDepartamento.Items.Add("Departamento ECU1");
                    ddlDepartamento.Items.Add("Departamento ECU2");
                    break;
                case "Perú":
                    //ddlDepartamento.Items.Clear();
                    ddlDepartamento.Items.Add("-- Seleccione --");
                    ddlDepartamento.Items.Add("Departamento PER1");
                    ddlDepartamento.Items.Add("Departamento PER2");
                    break;
                case "Rep. Dominicana":
                    //ddlDepartamento.Items.Clear();
                    ddlDepartamento.Items.Add("-- Seleccione --");
                    ddlDepartamento.Items.Add("Departamento RDO1");
                    ddlDepartamento.Items.Add("Departamento RDO2");
                    break;
            }
            //}
        }

        protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCiudad.Items.Clear();

            switch (ddlDepartamento.SelectedValue)
            {
                case "Antioquia":
                    //ddlDepartamento.Items.Clear();
                    ddlCiudad.Items.Add("-- Seleccione --");
                    ddlCiudad.Items.Add("Medellín");
                    ddlCiudad.Items.Add("Envigado");
                    break;
                case "Santander":
                    //ddlDepartamento.Items.Clear();
                    ddlCiudad.Items.Add("-- Seleccione --");
                    ddlCiudad.Items.Add("Bukaros");
                    ddlCiudad.Items.Add("Barranca");
                    break;

            }
        }
    }
}