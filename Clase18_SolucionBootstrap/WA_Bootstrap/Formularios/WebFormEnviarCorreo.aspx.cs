﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WA_Bootstrap.Formularios
{
    public partial class WebFormEnviarCorreo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com");

                smtpClient.Credentials = new NetworkCredential("y.delgado@ininfa.com", "Yetzo-0510A");
                smtpClient.EnableSsl = true;

                MailMessage mailMessage = new MailMessage();
                mailMessage.Subject = txtAsunto.Text;
                mailMessage.To.Add(new MailAddress(txtPara.Text));
                mailMessage.From = new MailAddress("y.delgado@ininfa.com", "Yesid");
                mailMessage.IsBodyHtml = true;

                mailMessage.Body = @"<h1 style='color:#008108'>" + txtContenido.Text + "</h1>";

                string archivo = Server.MapPath("~/Files/Document.docx");

                Attachment adjunto = new Attachment(archivo);

                mailMessage.Attachments.Add(adjunto);

                smtpClient.Send(mailMessage);

                lblInfo.CssClass = "text-success";
                lblInfo.Text = "Correo enviado exitosamente, por favor revise su bandeja";

                txtPara.Text = txtContenido.Text = txtAsunto.Text = "";

                //Buscar como:
                //-) adjuntar archivo
                //-) enviar texto enriquecido
            }
            catch (Exception ex)
            {
                lblInfo.CssClass = "text-danger";
                lblInfo.Text = $"error: {ex.Message}"; 
            }
        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtPara.Text = txtContenido.Text = txtAsunto.Text = "";
        }
    }
}