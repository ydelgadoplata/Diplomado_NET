﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WA_Bootstrap.Formularios
{
    public partial class WebFormBasico : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            lblInfo.Text = $"Nombre: {txtNombre.Text} {txtApellido.Text}";

            txtNombre.Text = txtApellido.Text = null;
        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {

        }
    }
}