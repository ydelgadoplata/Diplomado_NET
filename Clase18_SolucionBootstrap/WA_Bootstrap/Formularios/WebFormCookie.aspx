﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BootstrapMaster.Master" AutoEventWireup="true" CodeBehind="WebFormCookie.aspx.cs" Inherits="WA_Bootstrap.Formularios.WebFormCookie" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mt-3">
        <div class="card">
            <div class="card-header bg-primary text-white">
                ¡Lector Cookies!
            </div>
             <div class="card-body">
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Cookie Value:</label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="txtValorCookie" CssClass="form-control" PlaceHolder="Ingrese el valor del cookie" ToolTip="Aquí aparecerá el valor de la cookie"/>  
                    </div>
                </div>
               
                <div class="form-group row">
                    <div class="offset-md-2 col-md-4">
                        <asp:Button Text="Guardar" runat="server" ID="btnGuardar" CssClass="btn btn-success btn-block" OnClick="btnGuardar_Click"/>
                    </div>
                    <div class="offset-md-2 col-md-4">
                        <asp:Button Text="Leer" runat="server" ID="btnLeer" CssClass="btn btn-warning btn-block" OnClick="btnLeer_Click"/>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-md-2 col-md-3">
                        <asp:Label Text="" runat="server" ID="lblInfo" CssClass="text-success" />
                        <//div>
                    </div>
            </div>
       </div>
  </div>
</asp:Content>
