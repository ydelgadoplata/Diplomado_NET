﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BootstrapMaster.Master" AutoEventWireup="true" CodeBehind="WebFormBasico.aspx.cs" Inherits="WA_Bootstrap.Formularios.WebFormBasico" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mt-3">
        <div class="card">
            <div class="card-header bg-primary text-white">
                Formulario Básico
            </div>
            <div class="card-body">
                <asp:ValidationSummary runat="server" ID="vsResumen" DisplayMode="BulletList" ShowMessageBox="false" ShowSummary="true" CssClass="alert alert-danger"/>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtNombre">Nombre:</label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtNombre" Placeholder="Ingrese nombre" />
                        <asp:RequiredFieldValidator ErrorMessage="Nombre requerido" ControlToValidate="txtNombre" runat="server" ID="rfvNombre" Display="Dynamic" SetFocusOnError="true">&nbsp;</asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtApellido">Apellido:</label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtApellido" Placeholder="Ingrese apellido" />
                        <asp:RequiredFieldValidator ErrorMessage="Apellido requerido" ControlToValidate="txtApellido" runat="server" ID="rfvApellido" Display="Dynamic" SetFocusOnError="true">&nbsp;</asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-md-2 col-md-4">
                        <asp:Button Text="Enviar" runat="server" ID="btnEnviar" CssClass="btn btn-primary btn-block" OnClick="btnEnviar_Click" />
                    </div>
                    <div class="offset-md-2 col-md-4">
                        <asp:Button Text="Limpiar" runat="server" ID="btnLimpiar" CssClass="btn btn-outline-danger btn-block" OnClick="btnLimpiar_Click" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-md-2 col-md-3">
                        <asp:Label Text="" runat="server" ID="lblInfo" CssClass="text-success" />
                        <//div>
                    </div>
            </div>
       </div>
  </div>
</asp:Content>
