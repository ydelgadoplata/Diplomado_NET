﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WA_Bootstrap.Utilities
{
    public class Log
    {
        private string _encabezado;
        private string _fechaError;

        public Log()
        {
            _encabezado = DateTime.Now.ToShortDateString() + " ==> ";
            _fechaError = DateTime.Now.ToString("yyyy-MMM-dd");
        }

        public void ErrorLog(string nombreRuta, string mensajeError)
        {
            StreamWriter streamWriter = new StreamWriter(nombreRuta + _fechaError + ".txt", true);
            streamWriter.WriteLine(_encabezado + mensajeError);
            streamWriter.Flush();
            streamWriter.Close();
        }

    }
}