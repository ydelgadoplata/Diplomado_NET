﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //int i = 0;
                //int k = 10 / i;

                string dato = "dato a mostrar"; //si se pone valor nulo, entra en la excepcion
                Func(dato);

                Console.WriteLine(dato);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                
            }

            Console.ReadLine();
        }

        static void Func(string s)
        {
            if (s == null)
            {
                throw new Exception("El valor es nulo");
            }
        }
    }

}
