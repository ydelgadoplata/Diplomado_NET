﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    public class Padre
    {
        //guión bajo en las variables se emplea para definir variables globales.
        private int _PropPrivada { get; set; } //Privada, solo la vé el padre

        protected int _PropProtegida { get; set; } //Protegida, la hereda la hija, pero no la ve el program principal

        //primera letra de propiedades -> mayuscula, aplica en cada palabra compuesta
        public string Prop1Padre { get; set; }

        public int Prop2Padre { get; set; }

        public void MetodoPadre()
        {
            Console.WriteLine("Métido del Padre");
        }

    }
}
