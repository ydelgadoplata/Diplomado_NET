﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Hija:Padre
    {
        public int PropHija { get; set; }

        public void MetodoHija()
        {
            Console.WriteLine("Método de la clase hija");
        }
    }
}
