﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{
    class Linea : Figura
    {
        public override void Pintar ()
        {
            Console.WriteLine("Pinta una linea");
        }
    }
}
