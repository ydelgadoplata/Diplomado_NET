﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{
    class Circulo : Figura
    {
        public override void Pintar()
        {
            Console.WriteLine("Pinta un círculo");
        }
    }
}
