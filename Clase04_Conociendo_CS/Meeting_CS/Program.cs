﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meeting_CS
{
    class Program
    {
        static void Main(string[] args)
        {
            #region [Introducción]
            Console.WriteLine("Bienvenidos al primer aplicativo donde se aprecia concatenación e instanciamiento \n\nPresione ENTER para continuar");
            Console.ReadLine();

            #endregion

            #region [Instanciando persona con constructor sin parámetros]
            Person_class person1 = new Person_class();
            person1.FullName = "Colmillo de oso";
            //person1.Age = 45;
            person1.BirthDay = new DateTime(1975, 12, 25);
            person1.PersonGender = Gender.M;
            person1.Married = false;
            #endregion

            #region [Instanciando persona con constructor]
            Person_class person2 = new Person_class
            {
                FullName = "Camello",
                //Age = 25,
                BirthDay = new DateTime(1988, 5, 10),
                PersonGender = Gender.F,
                Married = true
            };
            #endregion

            #region [Escritura de Instancias 1 y 2]

            //concatenado 1
            //Console.WriteLine("Ejemplo de concatenación 1 e instanciamiento 1");
            Console.WriteLine("Ejemplo de concatenación 1 e instanciamiento 1\nNombre: " + person1.FullName + ", Edad: " + person1.Age + ", Fecha: " + person1.BirthDay.ToString("dd/MMM/yy") + ", Género: " + person1.PersonGender + ", E.Civil: " + person1.Married + "\n");
            //Concatenado 2
            //Console.WriteLine("Ejemplo de concatenación 2");
            Console.WriteLine("Ejemplo de concatenación 2 e instanciamiento 2\nName: {0}, Edad: {1}, Nacimiento: {2}, Género: {3}, E.Civil: {4}\n", person2.FullName, person2.Age, person2.BirthDay.ToString("dd/MM/yy"), person2.PersonGender, person2.Married);
            #endregion

            #region [Instanciando con constructor de 2 parámetros]
            Person_class person3 = new Person_class("Hoggy", 33);
            person3.PersonGender = Gender.F;
            person3.Married = true;
            //Console.WriteLine("Ejemplo de concatenación 3 e instanciamiento constructor 2 parámetros");
            Console.WriteLine($"Ejemplo de concatenación 3 e instanciamiento constructor 2 parámetros\nNombre: {person3.FullName}, Edad: {person3.Age}, Fecha: {person3.BirthDay.ToString("dd-MMM-yyyy")}, Género: {person3.PersonGender}, E.Civil: {person3.Married}\n");
            #endregion

            #region [Instanciando con constructor 3 parámetros]
            Person_class person4 = new Person_class("Lauren", 17, 2001, 6, 30);
            person4.PersonGender = Gender.F;
            person4.Married = false;
            string Info = String.Format("Nombre: {0}, Edad: {1}, Nacimiento: {2}\n", person4.FullName, person4.Age, person4.BirthDay.ToString("dd-MMM-yy"));
            Console.WriteLine("Ejemplo de concatenación, formato de fecha e instaciamiento constructor 3 parámetros\n{0}", Info);
            #endregion

            Console.WriteLine("{0}{0}{0}", Environment.NewLine); //3 espacios - Preguntar por environment.newline

            #region [ Nullables ]
            int? valor = null;

            if (!valor.HasValue)
            {
                valor = 10;
                Console.WriteLine("{0}\n", valor);
            } 
            #endregion

            person1.ShowStatus();

            person2.ShowStatus();

            person3.ShowStatus();

            person4.ShowStatus();

            Console.WriteLine("{0} {1}", person4.FullName, person4.Older() ? "es mayor de edad\n" : "es un crio\n");

            //int[] personlist = new int[] { person1., person2, Person3, person4 };
            //foreach (int element in Person_class)
            //{

            //}

            List<Person_class> People = new List<Person_class>
            {
                person1, person2, person3, person4
            };

            Console.WriteLine("Se hace el listado desde la clase, llamando al método\n");

            foreach (Person_class person in People)
            {
                person.ShowStatus();
            }


            Console.WriteLine("Gracias por revisar este código\n\nPulse ENTER para salir...");
            Console.ReadLine();
        }
    }
}
