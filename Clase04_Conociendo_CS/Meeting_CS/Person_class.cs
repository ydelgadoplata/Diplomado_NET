﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meeting_CS
{
    public class Person_class
    {
        public string FullName { get; set; }

        //public int Age { get; set; }
        public int Age
        {
            get
            {
                //int edad = DateTime.Today.AddTicks(-BirthDay.Ticks).Year - 1; //Nueva formad de calcular edad
                string actualyear = DateTime.Today.ToString("yyyy");
                int actyear = int.Parse(actualyear);
                int edad = actyear - BirthDay.Year;
                return edad;
            }
        }

        public DateTime BirthDay { get; set; }

        public bool? Married { get; set; }

        public Gender PersonGender { get; set; }

        public Person_class() //Snippet CTOR (Constructor)
        {

        }

        public Person_class(string nombre, int edad)
        {
            FullName = nombre;
            //Age = edad;
            BirthDay = DateTime.Now;
        }
        public Person_class(string nombre, int edad, int year, int month, int day)
        {
            FullName = nombre;
            //Age = edad;
            BirthDay = new DateTime(year, month, day);
        }

        public void ShowInfo1()
        {
            Console.WriteLine("Nombre: {0}, Edad: {1}\n", FullName, Age);
        }

        public void ShowStatus()
        {
            Console.WriteLine("{0}, tiene {1} años y {2}\n", FullName, Age, Married.Value ? "está casado" : "NO está casado");
            //if (Married == true)
            //{
            //    Console.WriteLine("Nombre {0}, tiene {1} años y está casado\n", FullName, Age);
            //}
            //else
            //{
            //    Console.WriteLine("Nombre {0}, tiene {1} años y NO está casado\n", FullName, Age);
            //}
        }

        public bool Older()
        {
            return Age >= 18;
        }

    }
}
