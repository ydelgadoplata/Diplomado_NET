﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConversionFormatos
{
    class Program
    {
        static void Main(string[] args)
        {
            #region [ Definicion TryParse en formatos numéricos ]
            string strnumero1;
            Console.WriteLine("Por favor digite un número:");
            strnumero1 = Console.ReadLine();
            //int numero1 = int.Parse(strnumero1);
            int numero1;
            bool esNumero = int.TryParse(strnumero1, out numero1);
            if (esNumero)
            {
                Console.WriteLine("El número ingresado es: " + numero1 + "\n");
            }
            else
            {
                Console.WriteLine("Lo digitado no es un numero!!!\n");
            }

            //double.TryParse
            //Convert.ToByte();
            //Byte.TryParse();
            //byte.TryParse();
            int numero2 = Convert.ToInt32(strnumero1);
            uint numero3 = Convert.ToUInt32(strnumero1);

            #endregion

            #region [Cadenas de caracteres]
            string nombre = "Enano de aguas";

            //longitud
            int longitud = nombre.Length;

            //Verificar si la cadena contiene el valor ingresado
            bool siContiene = nombre.Contains("an");

            //busca la posición del primer caracter a buscar en la cadena
            int indice = nombre.IndexOf('n');

            //busca la posición del ultimo caracter a buscar en la cadena
            int indice2 = nombre.LastIndexOf('n');

            //cortar parte de cadena
            string corte = nombre.Remove(3);

            //reemplazar caracter por otro
            string nueva = nombre.Replace('a', 'o');

            //quitar espacios
            string sinEspacios = nombre.Replace(" ","");

            //obtener un pedazo de la cadena
            string piece = nombre.Substring(2, 10);

            #endregion

            #region [Funciones matematicas]

            //Redondeo hacia arriba
            double resultArriba = Math.Floor(5.78);

            //Redondeo hacia abajo
            double resultAbajo = Math.Ceiling(5.78);

            //Potencia
            double exponencial = Math.Pow(2, 3);

            //raiz cuad
            double raiz = Math.Sqrt(9);

            //truncar
            double trunk = Math.Truncate(7.209);


            #endregion

            #region [ Encontrar cadena ]
            Console.WriteLine("Ejercicio:\nEncontrar dentro de una cadena de caracteres un texto específico.\nBuscar la página web dentro de la siguiente frase:\nEl driver (o sea el que programó antes) no encontró la ruta '/nolaencontreapesardetodo.html' o no ha sido implementada...\n");
            string ejerc = "El driver (o sea el que programó antes) no encontró la ruta '/nolaencontreapesardetodo.html' o no ha sido implementada...";
            //Encontrar el nombre de la pagina
            int inicio = ejerc.IndexOf('/');
            int final = ejerc.LastIndexOf('\'');
            string paginaerror = ejerc.Substring(inicio + 1, (final - inicio - 1));

            Console.WriteLine("La página con el error es: " + paginaerror + "\n"); 
            #endregion

            Console.WriteLine("Presione ENTER para salir");
            Console.ReadLine();
        }
    }
}
