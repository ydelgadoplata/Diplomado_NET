﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Calc
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Este programa permite sumar 2 numeros enteros y determinar si un numero es par o impar\nPresione Enter para continuar");
            Console.ReadLine();

            Console.WriteLine("Por favor, digite un número entero (N1)");
            string num1 = Console.ReadLine();
            int n1 = int.Parse(num1);
            Console.WriteLine("Por favor, digite un número entero (N2)");
            string num2 = Console.ReadLine();
            int n2 = int.Parse(num2);

            int resultsuma = Calculadora.Sum(n1, n2);
            int resultmulti = Calculadora.multi(n1, n2);
            Console.WriteLine("el resultado de la suma es: {0}\n", resultsuma);
            Console.WriteLine("El resultado del producto es: {0}\n", resultmulti);

            Console.WriteLine("Por favor, digite un número del 1 al 50");
            string num3 = Console.ReadLine();
            int n3 = int.Parse(num3);

            bool parnopar = Calculadora.par_impar(n3);
            Console.WriteLine("El número es {0}", parnopar ? "Par\n" : "Impar\n");
            Console.WriteLine("Presione ENTER para terminar");
            Console.ReadLine();
        }
    }
}
