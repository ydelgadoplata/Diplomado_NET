﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Calc
{
    public class Calculadora
    {
       public static int Sum(int op1, int op2)
        {
            return op1 + op2;
        }

        public static bool par_impar(int op3)
        {
            return op3 % 2 == 0;
        }

        public static int multi(int op1, int op2)
        {
            return op1 * op2;
        }

    }


}
