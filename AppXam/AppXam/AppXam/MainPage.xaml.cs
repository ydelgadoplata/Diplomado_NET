﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppXam
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        //async
        private void btnGetData_Clicked(object sender, EventArgs e)
        {
            //await DisplayAlert("Saludo", $"Hola {txtNombre.Text}, bienvenido!", "CERRAR");
            lblRespuesta.Text = $"!Hola {txtNombre.Text}, Bienvenido!";
        }
    }
}
