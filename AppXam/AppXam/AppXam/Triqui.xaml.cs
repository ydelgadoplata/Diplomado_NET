﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppXam
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Triqui : ContentPage
	{
        private Button[] botones = new Button[9];
        private GestionTriki gestionTriki = new GestionTriki();

        public Triqui ()
		{
			InitializeComponent ();

            botones[0] = btn1;
            botones[1] = btn2;
            botones[2] = btn3;
            botones[3] = btn4;
            botones[4] = btn5;
            botones[5] = btn6;
            botones[6] = btn7;
            botones[7] = btn8;
            botones[8] = btn9;


        }

        private void btn_Clicked(object sender, EventArgs e)
        {
            gestionTriki.setBoton(sender as Button);

            if (gestionTriki.CheckWinner(botones))
            {
                slGameOVer.IsVisible = true;

                foreach (Button button in botones)
                {
                    button.IsEnabled = false;
                }

            }
        }

        private void btnJugardeNuevo_Clicked(object sender, EventArgs e)
        {

        }
    }
}