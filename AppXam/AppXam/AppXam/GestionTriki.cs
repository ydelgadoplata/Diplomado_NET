﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace AppXam
{
    class GestionTriki
    {
        private int turnoJugador = 1;
        private int[,] Ganadores = new int[,]
        {
            {0,1,2}, {3,4,5}, {6,7,8}, {0,3,6}, {1,4,7}, {2,5,8}, {0,4,8}, {2,4,6},
        };

        public bool CheckWinner(Button[] botones)
        {
            bool gameOver = false;

            for (int i = 0; i < 8; i++)
            {
                int a = Ganadores[i, 0], b = Ganadores[i, 1], c = Ganadores[i, 2];

                Button b1 = botones[a], b2 = botones[b], b3 = botones[c];

                if (b1.Text == "" || b2.Text == "" || b3.Text == "")
                    continue;
                if (b1.Text == b2.Text && b2.Text == b3.Text)
                {
                    b1.BackgroundColor = b2.BackgroundColor = b3.BackgroundColor = Color.DarkBlue;
                    gameOver = true;
                    break;
                }

            }

            bool esEmpate = true;

            if (!gameOver)
            {
                foreach (Button b in botones)
                {
                    if (b.Text == "")
                    {
                        esEmpate = false;
                    }
                }

                if (esEmpate)
                {
                    gameOver = true;
                }
            }

            return gameOver;

        }

        public void setBoton(Button b)
        {
            if (b.Text == "")
            {
                b.Text = turnoJugador == 1 ? "X" : "O";
                turnoJugador = turnoJugador == 1 ? 2 : 1;
            }
        }

        public void ResetGame(Button[] botones)
        {
            turnoJugador = 1;

            foreach (Button b in botones)
            {
                b.Text = "";
                b.BackgroundColor = Color.Gray;
            }

        }

    }
}
