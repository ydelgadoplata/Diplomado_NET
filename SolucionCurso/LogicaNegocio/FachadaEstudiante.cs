﻿using AccesoDatos;
using AccesoDatos.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{
    public class FachadaEstudiante
    {
        private DBCursosEntities _contexto;


        public FachadaEstudiante() //Se crea una instancia llamando la entidad donde se tenga la BBDD
        {
            _contexto = new DBCursosEntities();

        }

        public int InsertarEstudiante(Estudiante estudiante)
        {

            if (String.IsNullOrEmpty(estudiante.Nombre))
            {
                throw new Exception("El nombre no puede ser vacío o nulo");
            }

            _contexto.Estudiantes.Add(estudiante);
            return _contexto.SaveChanges();
        }

        public Estudiante RetornarEstudiante(int cedula)
        {
            try
            {
                return _contexto.Estudiantes.Find(cedula); //Solo busca en la BBDD por la key de la tabla
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Estudiante> RetornarEstudiantes()
        {
            return _contexto.Estudiantes.OrderBy(e => e.Nombre).ToList();
            //return _contexto.Estudiantes.OrderBy(e => e.Nombre).Skip(2).Take(2).ToList(); Codigo utilizando Linq code
        }

        public List<EstudianteDTO> RetornarEstudiantesDTO(string nombre, string apellido, int ciudad)
        {
            var estudiantes = from e in _contexto.Estudiantes
                              select new EstudianteDTO
                              {
                                  Cedula = e.Cedula,
                                  NombreCompleto = e.Nombre + " " + e.Apellidos,
                                  NombreCiudadNacimiento = e.Ciudad.NombreCiudad,
                                  TextoGenero = e.Genero ? "M" : "F",
                                  //CiudadNacimiento = e.CiudadNacimiento
                              };

            if (!String.IsNullOrEmpty(nombre))
            {
                estudiantes = estudiantes.Where(e => e.NombreCompleto.Contains(nombre));
            }

            if (!String.IsNullOrEmpty(apellido))
            {
                estudiantes = estudiantes.Where(e => e.NombreCompleto.Contains(apellido));
            }

            if(ciudad != 0)
            {
                estudiantes = estudiantes.Where(e => e.CiudadNacimiento == ciudad);
            }

            return estudiantes.ToList();
        }

        public int EliminarEstudiante(int cedula)
        {

            Estudiante estudiante = _contexto.Estudiantes.Find(cedula);

            if (estudiante != null)
            {
                _contexto.Estudiantes.Remove(estudiante);
                return _contexto.SaveChanges();
            }

            return 0;
        }
    }
}
