﻿using AccesoDatos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{
    public class FachadaDepartamento
    {
        private DBCursosEntities _contextodepartamento;

        public FachadaDepartamento()
        {
            _contextodepartamento = new DBCursosEntities();
        }

        public Departamento RetornarDepartamento(int id)
        {
            return _contextodepartamento.Departamentoes.Find(id);
        }

        public int InsertarDepartamento(Departamento departamento)
        {
            if (_contextodepartamento.Departamentoes.Where(d => d.NombreDepartamento.ToLower() == departamento.NombreDepartamento.ToLower()).Count() > 0)
            {
                throw new Exception("Ya existe el departamento");
            }
            _contextodepartamento.Departamentoes.Add(departamento);
            return _contextodepartamento.SaveChanges();
        }

        public List<Departamento> RetornarDepartamentos(string nombreDepartamento)
        {
            var departamentos = from d in _contextodepartamento.Departamentoes
                                select d;
            if (!String.IsNullOrEmpty(nombreDepartamento))
            {
                departamentos = departamentos.Where(d => d.NombreDepartamento.Contains(nombreDepartamento));
            }
            return departamentos.ToList();

        }

        public int EliminarDepartamento(int id)
        {
            Departamento departamento = _contextodepartamento.Departamentoes.Find(id);

            if (departamento != null)
            {
                _contextodepartamento.Departamentoes.Remove(departamento);
                return _contextodepartamento.SaveChanges();
            }

            return 0;
        }

        public int ActDepartamento(Departamento departamento)
        {
            _contextodepartamento.Entry(departamento).State = EntityState.Modified;
            return _contextodepartamento.SaveChanges();
        }

    }
}
