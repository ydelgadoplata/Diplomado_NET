﻿using AccesoDatos;
using AccesoDatos.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace LogicaNegocio
{
    public class FachadaCiudad
    {
        private DBCursosEntities _ciudad;

        public FachadaCiudad()
        {
            _ciudad = new DBCursosEntities();
        }

        public List<Ciudad> RetornarCiudades()
        {
            return _ciudad.Ciudads.OrderBy(e => e.NombreCiudad).ToList();
        }

        public List<CiudadDTO> RetornarCiudadesDTO()
        {
            return _ciudad.Ciudads.OrderBy(c => c.NombreCiudad).Select(c => new CiudadDTO { Id = c.Id, NombreCiudad = c.NombreCiudad }).ToList();
        }

    }
}
