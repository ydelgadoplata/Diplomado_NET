﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSolucion.Master" AutoEventWireup="true" CodeBehind="WF_RolManagement.aspx.cs" Inherits="CursosWeb.Roles.WF_RolManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">
        <div class="card">
            <div class="card-header bg-primary text-white">
                Administración Roles
            </div>
            <div class="card-body">
                <div class="card-deck">
                    <!--Card de creacion de roles-->
                    <div class="card">
                        <div class="card-header bg-primary text-white">Crear Rol</div>
                        <div class="card-body mt-auto">
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="txtRol">Rol:</label>
                                    <div class="col-md-10">
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtRol" Placeholder="Escriba un nombre para el rol" />
                                        <%--<asp:RequiredFieldValidator ErrorMessage="Requerido*" ControlToValidate="txtRol" runat="server" ID="rfvRol" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                                        <asp:RegularExpressionValidator ErrorMessage="Sólo caractéres" ControlToValidate="txtRol" ValidationExpression="^[ A-Za-zÑñÁáÉéÍíÓóÚú]+$" CssClass="alert-danger" runat="server" />--%>
                                    </div>
                                </div>
                            <div class="form-group row" style="justify-content: center">
                                <div class="col-md-4">
                                    <asp:Button Text="Crear" runat="server" ID="btnCrear" CssClass="btn btn-success btn-block" OnClick="btnCrear_Click" />
                                </div>
                            </div>
                            <asp:Label Text="" runat="server" ID="lblInfoRol" CssClass="text-success" />
                        </div>
                    </div>

                    <!--Card asignacion de roles-->
                    <div class="card">
                        <div class="card-header bg-primary text-white">Asignar Rol</div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="txtUsuario">
                                    Usuario:
                                </label>
                                <div class="col-md-9">
                                    <asp:DropDownList CssClass="form-control" ID="ddlUsuario" runat="server">
                                    </asp:DropDownList>
                                    <%--<asp:RequiredFieldValidator ControlToValidate="ddlUsuario" ErrorMessage="Seleccione Usuario" ID="rfvUsuario" CssClass="alert-danger" Display="Dynamic" runat="server" />--%>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="txtRolSeleccionado">
                                    Rol:
                                </label>
                                <div class="col-md-9">
                                    <asp:DropDownList CssClass="form-control" ID="ddlRol" runat="server">
                                    </asp:DropDownList>
                                    <%--<asp:RequiredFieldValidator ControlToValidate="ddlRol" ErrorMessage="Seleccione Rol" ID="RequiredFieldValidator1" CssClass="alert-danger" Display="Dynamic" runat="server" />--%>
                                </div>
                            </div>
                            <div class="form-group row" style="justify-content: center">
                                <div class="col-md-4">
                                    <asp:Button Text="Asignar" runat="server" ID="btnAsignar" CssClass="btn btn-success btn-block" OnClick="btnAsignar_Click" />
                                </div>
                                <div class="col-md-4">
                                    <asp:Button Text="DesAsignar" runat="server" ID="btnDeasig" CssClass="btn btn-danger btn-block" OnClick="btnDeasig_Click" />
                                </div>
                            </div>
                            <asp:Label Text="" runat="server" ID="lblAsignRol" CssClass="text-success" />
                        </div>
                    </div>
                </div>

                <!--Grid de roles-->
                <div class="card mt-3">
                    <div class="card-header bg-primary text-white">Listado de roles</div>
                    <div class="card-body align-items-center">
                        <asp:GridView runat="server" ID="gvRoles" OnRowCommand="gvRoles_RowCommand" AutoGenerateColumns="false" CssClass="table table-primary table-hover table-bordered mt-3" AllowPaging="true" PageSize="5" OnPageIndexChanging="gvRoles_PageIndexChanging" HeaderStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundField HeaderText="Usuario" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Usuario" />
                                <asp:BoundField HeaderText="Rol" DataField="Rol" />
                                <%--<asp:BoundField HeaderText="País" DataField="NombrePais" />--%>
                                <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100 px">
                                    <ItemTemplate>
                                        <asp:Button Text="Editar" runat="server" ID="btnEditar" CssClass="btn btn-warning" CommandName="Editar" CommandArgument='<%# Bind ("Usuario") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
