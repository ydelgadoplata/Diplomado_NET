﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursosWeb.Roles
{
    public partial class WF_RolManagement : System.Web.UI.Page
    {
        RolManagement _rolManagement = new RolManagement();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LlenarUsarios();
                LlenarRoles();
                CargarRolesAsig();
            }

        }

        private void LlenarUsarios()
        {
            #region [ Llenar combo de usuarios]

            ddlUsuario.DataSource = _rolManagement.RetornarUsuarios();
            ddlUsuario.DataValueField = "UserName";
            ddlUsuario.DataTextField = "UserName";
            ddlUsuario.DataBind();

            ddlUsuario.Items.Insert(0, new ListItem("-- Seleccione usuario --", ""));
            #endregion
        }

        private void LlenarRoles()
        {
            #region [ Llenar combo de roles]

            ddlRol.DataSource = _rolManagement.RetornarRoles();
            ddlRol.DataValueField = "Name";
            ddlRol.DataTextField = "Name";
            ddlRol.DataBind();

            ddlRol.Items.Insert(0, new ListItem("-- Seleccione rol --", ""));
            #endregion
        }

        private void CargarRolesAsig()
        {
            gvRoles.DataSource = _rolManagement.RetornarUsuariosRol();
            gvRoles.DataBind();
        }


        protected void gvRoles_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void gvRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvRoles.PageIndex = e.NewPageIndex;
            CargarRolesAsig();
        }

        protected void btnCrear_Click(object sender, EventArgs e)
        {
            if (_rolManagement.CrearRol(txtRol.Text))
            {
                lblInfoRol.CssClass = "text-success";
                lblInfoRol.Text = "Rol creado exitosamente";
                txtRol.Text = "";
                LlenarRoles();
            }
            else
            {
                lblInfoRol.CssClass = "text-danger";
                lblInfoRol.Text = "Rol no pudo ser creado";
            }

        }

        protected void btnAsignar_Click(object sender, EventArgs e)
        {
            if (_rolManagement.AsignarRol(ddlUsuario.SelectedValue, ddlRol.SelectedValue))
            {
                lblAsignRol.CssClass = "text-success";
                lblAsignRol.Text = "Asignación exitosa";
                ddlRol.SelectedValue = ddlUsuario.SelectedValue = null;
                CargarRolesAsig();
            }
            else
            {
                lblAsignRol.CssClass = "text-danger";
                lblAsignRol.Text = "Asignación no exitosa";
            }
        }

        protected void btnDeasig_Click(object sender, EventArgs e)
        {
            if (_rolManagement.DesasignarRol(ddlUsuario.SelectedValue, ddlRol.SelectedValue))
            {
                lblAsignRol.CssClass = "text-success";
                lblAsignRol.Text = "Desasignación exitosa";
                ddlRol.SelectedValue = ddlUsuario.SelectedValue = null;
                CargarRolesAsig();
            }
            else
            {
                lblAsignRol.CssClass = "text-danger";
                lblAsignRol.Text = "Usuario no asignado a rol";
            }
        }
    }
}