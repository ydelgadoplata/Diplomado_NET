﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CursosWeb.Roles
{
    public class RolManagement
    {
        private UserStore<IdentityUser> _almacenarUsuarios;
        private UserManager<IdentityUser> _administrarUsuarios;

        private RoleStore<IdentityRole> _almacenarRoles;
        private RoleManager<IdentityRole> _administrarRoles;

        public RolManagement()
        {
            _almacenarUsuarios = new UserStore<IdentityUser>();
            _administrarUsuarios = new UserManager<IdentityUser>(_almacenarUsuarios);

            _almacenarRoles = new RoleStore<IdentityRole>();
            _administrarRoles = new RoleManager<IdentityRole>(_almacenarRoles);
            
        }

        public bool CrearRol(string nombreRol)
        {
            IdentityResult identityResult = _administrarRoles.Create(new IdentityRole(nombreRol));
            return identityResult.Succeeded;
        }

        public List<IdentityRole> RetornarRoles()
        {
            //llenar el dropdown DataTextField y el DataValueField se llena con Name
            return _administrarRoles.Roles.ToList();
        }

        public List<IdentityUser> RetornarUsuarios()
        {
            //llenar el dropdown DataTextField y el DataValueField se llena con UserName
            return _administrarUsuarios.Users.ToList();
        }

        public bool AsignarRol(string nombreUsuario, string nombreRol)
        {
            var usuario = _administrarUsuarios.FindByName(nombreUsuario);

            if (!_administrarUsuarios.IsInRole(usuario.Id, nombreRol))
            {
                IdentityResult identityResult = _administrarUsuarios.AddToRole(usuario.Id, nombreRol);
                return identityResult.Succeeded;
            }
            return false;
        }

        public bool DesasignarRol(string nombreUsuario, string nombreRol)
        {
            var usuario = _administrarUsuarios.FindByName(nombreUsuario);

            if (_administrarUsuarios.IsInRole(usuario.Id, nombreRol))
            {
                IdentityResult identityResult = _administrarUsuarios.RemoveFromRole(usuario.Id, nombreRol);
                return identityResult.Succeeded;
            }
            else
            {
                return false;
            }
        }

        public List<DTOUsuarioRol> RetornarUsuariosRol()
        {
            List<DTOUsuarioRol> usuariosRoles = _administrarUsuarios.Users.Select(u => new DTOUsuarioRol { Usuario = u.UserName, Rol = "" }).ToList();

            foreach (var usuario in usuariosRoles)
            {
                IdentityUser identityUser = _administrarUsuarios.FindByName(usuario.Usuario);

                foreach (var itemRol in identityUser.Roles)
                {
                    IdentityRole rol = _administrarRoles.FindById(itemRol.RoleId);
                    usuario.Rol += rol.Name + ",";
                }
            }

            return usuariosRoles;
        }

    }
}