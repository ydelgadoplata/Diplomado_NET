﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CursosWeb.Roles
{
    public class DTOUsuarioRol
    {
        public string Usuario { get; set; }

        public string Rol { get; set; }
    }
}