﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSolucion.Master" AutoEventWireup="true" CodeBehind="WF_ListarEstudiantes.aspx.cs" Inherits="CursosWeb.Estudiantes.WF_ListarEstudiantes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="../Content/Paginacion.css" rel="stylesheet" />

    <div class="form-inline mt-3">
        <div class="form-group">
            <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control mr-2" Placeholder="Nombre del estudiante" />
            <asp:TextBox runat="server" ID="txtApellido" CssClass="form-control mr-2" Placeholder="Apellido del estudiante" />
            <asp:DropDownList runat="server" ID="ddlCiudad" CssClass="form-control mr-2" />
            <asp:Button Text="Buscar" runat="server" ID="btnBuscar" CssClass="btn btn-primary" OnClick="btnBuscar_Click" />
        </div>
    </div>

    <asp:GridView runat="server" ID="gvEstudiantes" OnRowCommand="gvEstudiantes_RowCommand" AutoGenerateColumns="false" CssClass="table table-primary table-hover table-bordered mt-3" AllowPaging="true" PageSize="5" OnPageIndexChanging="gvEstudiantes_PageIndexChanging" HeaderStyle-HorizontalAlign="Center">
        <Columns>
            <asp:HyperLinkField DataTextField="Cedula" HeaderText="Cédula" DataNavigateUrlFormatString="~/Estudiantes/WF_Estudiante.aspx?Cedula={0}" DataNavigateUrlFields="Cedula" ItemStyle-HorizontalAlign="Center"/>
            <%--<asp:BoundField HeaderText="Cédula" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Cedula" />--%>
            <asp:BoundField HeaderText="Nombre Completo" DataField="NombreCompleto" />
            <asp:BoundField HeaderText="Ciudad Nacimiento" DataField="NombreCiudadNacimiento" />
            <asp:BoundField HeaderText="Género" DataField="TextoGenero" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
            
            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100 px">
                <ItemTemplate>
                    <asp:Button Text="Eliminar" runat="server" ID="btnEliminar" CssClass="btn btn-danger" OnClientClick="return confirm('¿Desea eliminar este registro?')" CommandArgument='<%# Bind ("Cedula") %>' CommandName="Eliminar" />
                </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
    </asp:GridView>
    <asp:Label Text="" runat="server" ID="lblConfirmacion" CssClass="text-success" />
</asp:Content>
