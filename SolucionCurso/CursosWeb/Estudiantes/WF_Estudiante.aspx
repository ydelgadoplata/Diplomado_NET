﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSolucion.Master" AutoEventWireup="true" CodeBehind="WF_Estudiante.aspx.cs" Inherits="CursosWeb.Estudiantes.WF_Estudiante" %>

<asp:Content ID="FormEstudiantes" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<asp:HyperLink runat="server" ID="hlIngles" NavigateUrl="?lang=es" Text="<%$Resources:multilenguaje, lang%>" />
    <asp:HyperLink runat="server" ID="hlEspanol" NavigateUrl="?lang=en" Text="<%$Resources:multilenguaje, lang%>" />--%>

    <div class="mt-1 row" style="justify-content:flex-end">
        <div class="col-sm-1">
            <asp:ImageButton ImageUrl="~/Images/USA-icon.png" runat="server" ID="btnEng" OnClick="btnEng_Click" ToolTip="English language" CausesValidation="false"/>
            <asp:ImageButton ImageUrl="~/Images/Colombia-icon.png" runat="server" ID="btnEsp" OnClick="btnEsp_Click" ToolTip="Español" CausesValidation="false"/>
        </div>
    </div>
    
    <div class="mt-1">
        <div class="card">
            <div class="card-header bg-primary text-white" ID="encabezado">
                Estudiante
            </div>
            <div class="card-body">
                <asp:validationsummary runat="server" id="vsResumen" displaymode="BulletList" showmessagebox="false" showsummary="true" cssclass="alert alert-danger" />

                <!-- Cedula -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtCedula">
                        <asp:Literal Text="<%$Resources:multilenguaje, cedula%>" runat="server" />
                    </label>
                    <div class="col-md-8">
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtCedula" Placeholder="Ingrese número de documento" />
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ErrorMessage="Doc. requerido" ControlToValidate="txtCedula" runat="server" ID="rfvCedula" CssClass="alert-danger" Display="Dynamic" SetFocusOnError="true" />
                        <asp:CompareValidator ErrorMessage="Digite sólo números" CssClass="alert-danger" ControlToValidate="txtCedula" Operator="DataTypeCheck" Type="Integer" runat="server" />
                    </div>
                </div>

                <!-- Nombre -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtNombre">
                        <asp:Literal Text="<%$Resources:multilenguaje, nombre%>" runat="server" />
                    </label>
                    <div class="col-md-8">
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtNombre" Placeholder="Ingrese nombre" />
                    </div>
                    <%--<asp:CompareValidator ErrorMessage="Digite sólo caracteres" CssClass="alert-danger" ControlToValidate="txtNombre" Operator="DataTypeCheck" Type="String" runat="server" />--%>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ErrorMessage="Nombre requerido" ControlToValidate="txtNombre" runat="server" ID="rfvNombre" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                        <asp:RegularExpressionValidator ErrorMessage="Sólo caractéres" ControlToValidate="txtNombre" ValidationExpression="^[ A-Za-zÑñ]*$" CssClass="alert-danger" runat="server" />
                    </div>
                </div>

                <!-- Apellido -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtApellido">
                        <asp:Literal Text="<%$Resources:multilenguaje, apellido%>" runat="server" />
                    </label>
                    <div class="col-md-8">
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtApellido" Placeholder="Ingrese apellido" />
                    </div>
                    <div class="col-md-2">
                        <asp:RegularExpressionValidator ErrorMessage="Sólo caractéres" ControlToValidate="txtApellido" ValidationExpression="^[ A-Za-zÑñ]*$" CssClass="alert-danger" runat="server" />
                    </div>
                </div>

                <!-- Ciudad -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtCiudad">
                        <asp:Literal Text="<%$Resources:multilenguaje, ciudad%>" runat="server" />
                    </label>
                    <div class="col-md-8">
                        <asp:DropDownList CssClass="form-control" ID="ddlCiudad" runat="server">
                            <%--<asp:ListItem Text=" -- Seleccione Ciudad -- " Value=NULL />--%>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ControlToValidate="ddlCiudad" ErrorMessage="Debe seleccionar una ciudad" ID="rfvCiudad" CssClass="alert-danger" Display="Dynamic" runat="server" />
                    </div>
                </div>
                <%--\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*--%>

                <!-- Correo -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtCorreo">
                        <asp:Literal Text="<%$Resources:multilenguaje, correo%>" runat="server" />
                    </label>
                    <div class="col-md-8">
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtCorreo" Placeholder="Ingrese correo electrónico" />
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ErrorMessage="Correo requerido" ControlToValidate="txtCorreo" runat="server" ID="rfvCorreo" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                        <asp:RegularExpressionValidator ErrorMessage="El correo electrónico no es válido" ControlToValidate="txtCorreo" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="alert-danger" runat="server" />
                    </div>
                </div>

                <!-- Género -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">
                        <asp:Literal Text="<%$Resources:multilenguaje, genero%>" runat="server" />
                    </label>
                    <div class="col-md-10">
                        <asp:RadioButtonList runat="server" ID="rdoGenero">
                            <asp:ListItem Text="<%$Resources:multilenguaje, femenino%>" Value="false" Selected="True"/>
                            <asp:ListItem Text="<%$Resources:multilenguaje, masculino%>" Value="true"/>
                        </asp:RadioButtonList>
                    </div>
                </div>

                <!-- Botones -->
                <div class="form-group row">
                    <div class="offset-md-2 col-md-4">
                        <asp:Button Text="<%$Resources:multilenguaje, crear%>" runat="server" ID="btnCrear" CssClass="btn btn-primary btn-block" OnClick="btnCrear_Click" />
                    </div>
                    <div class="offset-md-2 col-md-4">
                        <asp:Button Text="<%$Resources:multilenguaje, limpiar%>" runat="server" CausesValidation="false" ID="btnLimpiar" CssClass="btn btn-outline-danger btn-block" OnClick="btnLimpiar_Click" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-md-1 col-md-10">
                        <asp:label text="" runat="server" id="lblInfo" cssclass="text-success" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
