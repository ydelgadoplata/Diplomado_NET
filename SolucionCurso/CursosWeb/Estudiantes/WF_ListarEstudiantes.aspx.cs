﻿using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursosWeb.Estudiantes
{
    public partial class WF_ListarEstudiantes : System.Web.UI.Page
    {
        private FachadaEstudiante _fachadaEstudiante = new FachadaEstudiante();
        private FachadaCiudad _fachadaCiudad = new FachadaCiudad();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargarCiudades();
                CargarEstudiante();
                
            }
        }

        private void CargarCiudades()
        {
            ddlCiudad.DataSource = _fachadaCiudad.RetornarCiudades();
            ddlCiudad.DataValueField = "Id";
            ddlCiudad.DataTextField = "NombreCiudad";
            ddlCiudad.DataBind();
            ddlCiudad.Items.Insert(0, new ListItem(" -- Seleccione ciudad -- ", "0"));
        }

        private void CargarEstudiante()
        {
            gvEstudiantes.DataSource = _fachadaEstudiante.RetornarEstudiantesDTO(txtNombre.Text, txtApellido.Text, Convert.ToInt32(ddlCiudad.SelectedValue));
            gvEstudiantes.DataBind();
        }

        protected void gvEstudiantes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvEstudiantes.PageIndex = e.NewPageIndex;
            CargarEstudiante();
        }

        protected void gvEstudiantes_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Eliminar")
            {
                int cedula = Convert.ToInt32(e.CommandArgument);

                if (_fachadaEstudiante.EliminarEstudiante(cedula) == 1)
                {
                    CargarEstudiante();
                    lblConfirmacion.CssClass = "text-success";
                    lblConfirmacion.Text = "Registro eliminado";
                }
                else
                {
                    lblConfirmacion.CssClass = "text-danger";
                    lblConfirmacion.Text = "El registro no se pudo eliminar";
                }

            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            CargarEstudiante();
        }
    }
}