﻿using AccesoDatos;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursosWeb.Estudiantes
{
    public partial class WF_Estudiante : BasePage // System.Web.UI.Page
    {
        FachadaEstudiante _fachadaEstudiante = new FachadaEstudiante();
        FachadaCiudad fachadaCiudad = new FachadaCiudad();
        Estudiante estudiante = new Estudiante();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region [ Llenar combo de Ciudad ]
                FachadaCiudad fachadaCiudad = new FachadaCiudad();

                ddlCiudad.DataSource = fachadaCiudad.RetornarCiudades();
                ddlCiudad.DataValueField = "Id";
                ddlCiudad.DataTextField = "NombreCiudad";
                ddlCiudad.DataBind();

                ddlCiudad.Items.Insert(0, new ListItem("-- Seleccione una ciudad --", ""));
                #endregion

                if (!String.IsNullOrEmpty(Request["Cedula"]))
                {
                    CargarEstudiante(Convert.ToInt32(Request["Cedula"]));
                    
                }

                //if (string.IsNullOrEmpty(Convert.ToString(Session["lang"])))
                //{
                //    hlIngles.Visible = false;
                //    hlEspanol.Visible = true;
                //}
                //else
                //{
                //    string lang = Session["lang"].ToString();

                //    if (lang.Equals("es"))
                //    {
                //        hlIngles.Visible = false;
                //        hlEspanol.Visible = true;
                //    }
                //    else
                //    {
                //        hlIngles.Visible = true;
                //        hlEspanol.Visible = false;
                //    }
                //}
            }
        }

        private void CargarEstudiante(int cedula)
        {
            try
            {
                Estudiante estudiante = _fachadaEstudiante.RetornarEstudiante(cedula);

                if (estudiante != null)
                {
                    txtCedula.Text = cedula.ToString();
                    txtCedula.Enabled = false;

                    txtNombre.Text = estudiante.Nombre;
                    txtApellido.Text = estudiante.Apellidos;
                    ddlCiudad.SelectedValue = estudiante.CiudadNacimiento.ToString();
                    txtCorreo.Text = estudiante.Correo;
                    rdoGenero.SelectedValue = estudiante.Genero.ToString().ToLower();

                    btnCrear.Text = "Actualizar";
                }
                else
                {
                    lblInfo.CssClass = "text-danger";
                    lblInfo.Text = $"El estudiante con la cédula {cedula} no existe";
                }
            }
            catch (Exception ex)
            {
                lblInfo.CssClass = "text-danger";
                lblInfo.Text = ex.Message;
            }
        }


        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtCedula.Text = txtNombre.Text = txtApellido.Text = txtCorreo.Text = null;
            rdoGenero.SelectedValue = "false";
            ddlCiudad.SelectedValue = null;
        }

        protected void btnCrear_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    Estudiante estudiante = new Estudiante

                    {
                        Cedula = Convert.ToInt32(txtCedula.Text),
                        Nombre = txtNombre.Text,
                        Apellidos = txtApellido.Text,
                        Correo = txtCorreo.Text,
                        Genero = Convert.ToBoolean(rdoGenero.SelectedValue),
                        CiudadNacimiento = Convert.ToInt32(ddlCiudad.SelectedValue)
                    };

                    FachadaEstudiante fachadaEstudiante = new FachadaEstudiante();

                    //fachadaEstudiante.InsertarEstudiante(estudiante);

                    if (fachadaEstudiante.InsertarEstudiante(estudiante) == 1)
                    {
                        lblInfo.CssClass = "text-success";
                        lblInfo.Text = "El estudiante se ingresó exitosamente";
                        txtCedula.Text = txtNombre.Text = txtApellido.Text = txtCorreo.Text = null;
                        rdoGenero.SelectedValue = "false";
                        ddlCiudad.SelectedValue = "default";
                    }
                    else
                    {
                        lblInfo.CssClass = "text-danger";
                        lblInfo.Text = "El estudiante no se pudo ingresar";
                    }
                }
            }
            catch (Exception ex)
            {
                lblInfo.CssClass = "text-danger";
                lblInfo.Text = ex.Message;
            }
        }

        protected void btnEsp_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect($"~/Estudiantes/WF_Estudiante.aspx?lang=es");
        }

        protected void btnEng_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect($"~/Estudiantes/WF_Estudiante.aspx?lang=en");
        }
    }
}
