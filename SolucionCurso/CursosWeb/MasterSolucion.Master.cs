﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursosWeb
{
    public partial class MasterSolucion : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lgsLogout_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            var administracionAutenticacion = HttpContext.Current.GetOwinContext().Authentication;
            administracionAutenticacion.SignOut();
            Response.Redirect("~/Default.aspx");
        }
    }
}