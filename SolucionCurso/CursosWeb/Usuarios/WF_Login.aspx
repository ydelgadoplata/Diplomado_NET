﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSolucion.Master" AutoEventWireup="true" CodeBehind="WF_Login.aspx.cs" Inherits="CursosWeb.Usuarios.WF_Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">
        <div class="card">
            <div class="card-header text-white bg-primary">
                Iniciar Sesión
            </div>
            <div class="card-body">

                <asp:PlaceHolder runat="server" ID="phEstadoLogin" Visible="false">
                    <p>
                        <asp:Literal runat="server" ID="lblEstado" />
                    </p>
                </asp:PlaceHolder>

                <asp:PlaceHolder runat="server" ID="phFormularioLogin">
                    <asp:ValidationSummary runat="server" ID="vsResumen" DisplayMode="List" ShowMessageBox="false" ShowSummary="true" CssClass="alert alert-danger" />

                    <!-- Usuario -->
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label" for="txtUsuario">Usuario:</label>
                        <div class="col-md-8">
                            <asp:TextBox runat="server" CssClass="form-control" ID="txtUsuario" placeholder="Ingrese correo electrónico" />
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ErrorMessage="Usuario requerido" ControlToValidate="txtUsuario" runat="server" ID="rfvUsuario" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                            <asp:RegularExpressionValidator ErrorMessage="El usuario no es válido" ControlToValidate="txtUsuario" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="alert-danger" runat="server" />
                        </div>
                    </div>

                    <!-- Password -->
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label" for="txtPassword">Contraseña:</label>
                        <div class="col-md-8">
                            <asp:TextBox runat="server" CssClass="form-control" ID="txtPassword" placeholder="Ingrese contraseña" TextMode="Password" />
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ErrorMessage="Ingrese contraseña" ControlToValidate="txtPassword" runat="server" ID="rfvPassword" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                        </div>
                    </div>

                    <!-- Botones -->
                    <div class="form-group row" style="justify-content:center">
                        <div class="col-md-3">
                            <asp:Button Text="Login" runat="server" ID="btnLogin" CssClass="btn btn-primary btn-block" OnClick="btnLogin_Click"/>
                        </div>
                        <%--<div class="offset-md-2 col-md-4">
                        <asp:button Text="Limpiar" runat="server" CausesValidation="false" id="btnLimpiar" CssClass="btn btn-outline-danger btn-block" OnClick="btnLimpiar_Click" />
                    </div>--%>
                    </div>
                    <div class="form-group row">
                        <div class="offset-md-1 col-md-10">
                            <asp:Label Text="" runat="server" ID="lblInfo" CssClass="text-success" />
                        </div>
                    </div>

                </asp:PlaceHolder>

                <asp:PlaceHolder runat="server" ID="phCerrarSesion" Visible="false">
                    <div class="col-md-4 offset-md-4">
                        <asp:Button Text="Logout" runat="server" ID="btnLogout" CssClass="btn btn-primary btn-block" OnClick="btnLogout_Click" />
                    </div>
                </asp:PlaceHolder>


            </div>
        </div>
    </div>

</asp:Content>
