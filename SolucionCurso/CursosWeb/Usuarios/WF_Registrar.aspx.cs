﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursosWeb.Usuarios
{
    public partial class WF_Registrar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCrear_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                var almacenamientoUsuarios = new UserStore<IdentityUser>();
                var administrar = new UserManager<IdentityUser>(almacenamientoUsuarios);
                var usuario = new IdentityUser { UserName = txtUsuario.Text };

                IdentityResult resultado = administrar.Create(usuario, txtPassword.Text);

                if (resultado.Succeeded)
                {
                    lblInfo.CssClass = "text-success";
                    lblInfo.Text = $"El usuario {usuario.UserName} fue creado exitosamente";
                    txtUsuario.Text = "";
                }
                else
                {
                    lblInfo.CssClass = "text-danger";
                    lblInfo.Text = resultado.Errors.FirstOrDefault();
                }
                
            }
        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {

        }
    }
}