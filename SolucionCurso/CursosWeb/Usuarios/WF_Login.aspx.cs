﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursosWeb.Usuarios
{
    public partial class WF_Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (User.Identity.IsAuthenticated)
                {
                    lblEstado.Text = $"Bienvenido, {User.Identity.GetUserName()}";
                    phEstadoLogin.Visible = phCerrarSesion.Visible = true;
                    phFormularioLogin.Visible = false;
                }
                else
                {
                    phEstadoLogin.Visible = phCerrarSesion.Visible = false;
                    phFormularioLogin.Visible = true;

                }
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                var userStore = new UserStore<IdentityUser>();
                var userManager = new UserManager<IdentityUser>(userStore);
                var user = userManager.Find(txtUsuario.Text, txtPassword.Text);

                if (user != null)
                {
                    var adminAutenticacion = HttpContext.Current.GetOwinContext().Authentication;
                    var identidadUsuario = userManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);

                    adminAutenticacion.SignIn(new AuthenticationProperties() { IsPersistent = false }, identidadUsuario);

                    Response.Redirect("~/Usuarios/WF_Login.aspx");
                }
                else
                {
                    lblInfo.CssClass = "text-danger";
                    lblInfo.Text = "Usuario o clave inválidos";
                }

            }
            catch (Exception ex)
            {
                lblInfo.Text = ex.Message;
                lblInfo.CssClass = "text-danger";
            }
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            var administracionAutenticacion = HttpContext.Current.GetOwinContext().Authentication;
            administracionAutenticacion.SignOut();
            Response.Redirect("~/Usuarios/WF_Login.aspx");
        }
    }
}