﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSolucion.Master" AutoEventWireup="true" CodeBehind="WF_Registrar.aspx.cs" Inherits="CursosWeb.Usuarios.WF_Registrar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">
        <div class="card">
            <div class="card-header bg-primary text-white">
                Registrar
            </div>
            <div class="card-body">
                <asp:validationsummary runat="server" id="vsResumen" displaymode="List" showmessagebox="false" showsummary="true" cssclass="alert alert-danger" />

                <!-- Usuario -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtUsuario">Usuario:</label>
                    <div class="col-md-8">
                        <asp:textbox runat="server" cssclass="form-control" id="txtUsuario" placeholder="Ingrese correo electrónico" />
                    </div>
                    <div class="col-md-2">
                        <asp:requiredfieldvalidator errormessage="Usuario requerido" controltovalidate="txtUsuario" runat="server" id="rfvUsuario" display="Dynamic" setfocusonerror="true" cssclass="alert-danger" />
                        <asp:regularexpressionvalidator errormessage="El usuario no es válido" controltovalidate="txtUsuario" validationexpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" cssclass="alert-danger" runat="server" />
                    </div>
                </div>

                <!-- Password -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtPassword">Contraseña:</label>
                    <div class="col-md-8">
                        <asp:textbox runat="server" cssclass="form-control" id="txtPassword" placeholder="Ingrese contraseña" textmode="Password" />
                    </div>
                    <div class="col-md-2">
                        <asp:requiredfieldvalidator errormessage="Ingrese contraseña" controltovalidate="txtPassword" runat="server" id="rfvPassword" display="Dynamic" setfocusonerror="true" cssclass="alert-danger" />
                    </div>
                </div>

                <!-- Confirmar Password -->
                <div class="form-group row align-items-center">
                    <label class="col-md-2 col-form-label" for="txtConfPassword">Confirmar Contraseña:</label>
                    <div class="col-md-8">
                        <asp:textbox runat="server" CssClass="form-control" ID="txtConfPassword" Placeholder="Confirme contraseña" TextMode="Password" />
                    </div>
                    <div class="col-md-2">
                        <asp:requiredfieldvalidator ErrorMessage="Ingrese contraseña" ControlToValidate="txtConfPassword" runat="server" ID="rfvConfirmarPass" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                        <asp:CompareValidator ErrorMessage="Las contraseñas no coinciden" ControlToValidate="txtConfPassword" ControlToCompare="txtPassword" runat="server" CssClass="alert-danger"/>
                    </div>
                </div>

                <!-- Botones -->
                <div class="form-group row" style="justify-content:center">
                    <div class="col-md-3">
                        <asp:button Text="Crear" runat="server" id="btnCrear" CssClass="btn btn-primary btn-block" OnClick="btnCrear_Click" />
                    </div>
                </div>
                <!-- Label de info -->
                <div class="form-group row">
                    <div class="offset-md-2 col-md-3">
                        <asp:label Text="" runat="server" ID="lblInfo" CssClass="text-success" />
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
