﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSolucion.Master" AutoEventWireup="true" CodeBehind="WF_ListarDepartamentos.aspx.cs" Inherits="CursosWeb.Departamentos.WF_ListarDepartamentos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../Content/Paginacion.css" rel="stylesheet" />

    <div class="form-inline mt-3">
        <div class="form-group">
            <asp:TextBox runat="server" ID="txtDepartamento" CssClass="form-control mr-2" Placeholder="Nombre departamento" />
            <asp:Button Text="Buscar" runat="server" ID="btnBuscar" CssClass="btn btn-primary" OnClick="btnBuscar_Click" />
        </div>
    </div>

    <asp:GridView runat="server" ID="gvDepartamentos" OnRowCommand="gvDepartamentos_RowCommand" AutoGenerateColumns="false" CssClass="table table-primary table-hover table-bordered mt-3" AllowPaging="true" PageSize="5" OnPageIndexChanging="gvDepartamentos_PageIndexChanging" HeaderStyle-HorizontalAlign="Center">
        <Columns>
            <asp:BoundField HeaderText="Id" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Id" />
            <asp:BoundField HeaderText="Departamento" DataField="NombreDepartamento" />
            <%--<asp:BoundField HeaderText="País" DataField="NombrePais" />--%>
            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100 px">
                <ItemTemplate>
                    <asp:Button Text="Editar" runat="server" ID="btnEditar" CssClass="btn btn-warning" CommandName="Editar" CommandArgument='<%# Bind ("Id") %>'/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100 px">
                <ItemTemplate>
                    <asp:Button Text="Eliminar" runat="server" ID="btnEliminar" CssClass="btn btn-danger" OnClientClick="return confirm('¿Desea eliminar este registro?')" CommandArgument='<%# Bind ("Id") %>' CommandName="Eliminar" />
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
    </asp:GridView>
    <asp:Label Text="" runat="server" ID="lblConfirmacion" CssClass="text-success" />

</asp:Content>