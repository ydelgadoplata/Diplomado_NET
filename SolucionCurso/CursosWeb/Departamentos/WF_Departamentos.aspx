﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSolucion.Master" AutoEventWireup="true" CodeBehind="WF_Departamentos.aspx.cs" Inherits="CursosWeb.Departamentos.WF_Departamentos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">
        <div class="card">
            <div class="card-header bg-primary text-white">
                Departamento
            </div>
            <div class="card-body">
                <asp:ValidationSummary runat="server" ID="vsResumen" DisplayMode="List" ShowMessageBox="false" ShowSummary="true" CssClass="alert alert-danger" />
                <!-- Id -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtIdDepa">Id:</label>
                    <div class="col-md-8">
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtId" Placeholder="Id automático" Enabled="false" />
                    </div>
                </div>

                <!-- Departamento -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtDepartamento">Departamento:</label>
                    <div class="col-md-8">
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtDepartamento" Placeholder="Ingrese nombre departamento" />
                    </div>
                    <%--<asp:CompareValidator ErrorMessage="Digite sólo caracteres" CssClass="alert-danger" ControlToValidate="txtNombre" Operator="DataTypeCheck" Type="String" runat="server" />--%>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ErrorMessage="Departamento requerido" ControlToValidate="txtDepartamento" runat="server" ID="rfvDepartamento" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                        <asp:RegularExpressionValidator ErrorMessage="Sólo caractéres" ControlToValidate="txtDepartamento" ValidationExpression="^[ A-Za-zÑñÁáÉéÍíÓóÚú]+$" CssClass="alert-danger" runat="server" />
                    </div>
                </div>
                <!-- Botones -->
                <div class="form-group row">
                    <div class="offset-md-2 col-md-4">
                        <asp:Button Text="Crear" runat="server" ID="btnCrear" CssClass="btn btn-primary btn-block" OnClick="btnCrear_Click" />
                    </div>
                    <div class="offset-md-2 col-md-4">
                        <asp:Button Text="Limpiar" runat="server" CausesValidation="false" ID="btnLimpiar" CssClass="btn btn-outline-danger btn-block" OnClick="btnLimpiar_Click" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-md-1 col-md-10">
                        <asp:Label Text="" runat="server" ID="lblInfo" CssClass="text-success" />
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
