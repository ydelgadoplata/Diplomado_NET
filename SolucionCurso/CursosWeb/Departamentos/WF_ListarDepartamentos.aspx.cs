﻿using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursosWeb.Departamentos
{
    public partial class WF_ListarDepartamentos : System.Web.UI.Page
    {
        private FachadaDepartamento _fachadaDepartamento = new FachadaDepartamento();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargarDepartamentos();
            }
        }

        private void CargarDepartamentos()
        {
            gvDepartamentos.DataSource = _fachadaDepartamento.RetornarDepartamentos(txtDepartamento.Text);
            gvDepartamentos.DataBind();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            CargarDepartamentos();
        }

        protected void gvDepartamentos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvDepartamentos.PageIndex = e.NewPageIndex;
            CargarDepartamentos();
            
        }

        protected void gvDepartamentos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int id = Convert.ToInt32(e.CommandArgument);

            if (e.CommandName == "Eiminar")
            {
                
                if (_fachadaDepartamento.EliminarDepartamento(id) == 1)
                {
                    CargarDepartamentos();

                    lblConfirmacion.CssClass = "text-success";
                    lblConfirmacion.Text = "Registro eliminado con éxito";
                }

                else
                {
                    lblConfirmacion.CssClass = "text-danger";
                    lblConfirmacion.Text = "El registro no se pudo eliminar";
                }
            }

            if (e.CommandName == "Editar")
            {
                Response.Redirect($"~/Departamentos/WF_Departamentos.aspx?Id={id}");
            }

        }
    }
}