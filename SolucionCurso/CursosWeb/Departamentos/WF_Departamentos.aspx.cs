﻿using AccesoDatos;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursosWeb.Departamentos
{
    public partial class WF_Departamentos : System.Web.UI.Page
    {
        private FachadaDepartamento _fachadaDepartamento = new FachadaDepartamento();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request["Id"]))
                {
                    CargarDepartamento(Convert.ToInt32(Request["Id"]));
                }

            }

        }

        private void CargarDepartamento(int id)
        {
            try
            {
                Departamento departamento = _fachadaDepartamento.RetornarDepartamento(id);

                if (departamento != null)
                {

                    txtId.Text = id.ToString();
                    txtId.Enabled = false;

                    txtDepartamento.Text = departamento.NombreDepartamento;

                    btnCrear.Text = "Actualizar";
                }
                else
                {
                    lblInfo.CssClass = "text-danger";
                    lblInfo.Text = $"El registro {id} no existe";
                }
            }
            catch (Exception ex)
            {
                ControlExcepcion(ex);
            }
        }

        private void OperacionExitosa()
        {
            txtDepartamento.Text = null;

            lblInfo.CssClass = "text-success";
            lblInfo.Text = "El registro se realizó exitosamente";
        }

        private void ControlExcepcion(Exception ex)
        {
            lblInfo.CssClass = "text-danger";
            lblInfo.Text = ex.Message;
        }

        protected void btnCrear_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    Departamento departamento = new Departamento
                    {
                        NombreDepartamento = txtDepartamento.Text
                    };

                    if (btnCrear.Text == "Actualizar")
                    {
                        departamento.Id = Convert.ToInt32(txtId.Text);

                        if (_fachadaDepartamento.ActDepartamento(departamento) == 1)
                        {
                            Response.Redirect("~/Departamentos/WF_ListarDepartamentos.aspx");
                        }
                        else
                        {
                            lblInfo.CssClass = "text-danger";
                            lblInfo.Text = "El departamento no se pudo actualizar";
                        }
                    }
                    else
                    {
                        if (_fachadaDepartamento.InsertarDepartamento(departamento) == 1)
                        {
                            txtDepartamento.Text = "";

                            lblInfo.CssClass = "text-success";
                            lblInfo.Text = "El departamento se ingresó exitosamente";

                        }
                        else
                        {
                            lblInfo.CssClass = "text-danger";
                            lblInfo.Text = "El departamento no se pudo ingresar";

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ControlExcepcion(ex);
            }
        }


        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtDepartamento.Text = "";
        }
    }
}