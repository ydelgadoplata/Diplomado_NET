﻿using ClienteServiciosCursos.ServicioWebCurso;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ClienteServiciosCursos
{
    public partial class WF_ConsumoServicio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            WSCursos wSCursos = new WSCursos();

            gvCiudades.DataSource = wSCursos.ListadoCiudades();
            gvCiudades.DataBind();
        }
    }
}