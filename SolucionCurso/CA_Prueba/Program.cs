﻿using AccesoDatos;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursosConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                FachadaEstudiante fachadaEstudiante = new FachadaEstudiante();
                FachadaCiudad fachadaCiudad = new FachadaCiudad();

                #region [ Ingreso estudiante ]
                //Estudiante estudiante = new Estudiante
                //{
                //    Cedula = 91508000, //Campo numérico - REQ
                //    Nombre = "Yesid", //Nombre - REQ
                //    Apellidos = "Delgado", //Apellido NO REQ
                //    CiudadNacimiento = 16, //Dropdown list - REQ
                //    Correo = "ydelgadoplata@gmail.com", // Correo - Verficar
                //    Genero = true //Dropdown Seleccion - REQ
                //};

                //int numRegistrado = fachadaEstudiante.InsertarEstudiante(estudiante);

                //if (numRegistrado == 1)
                //{
                //    Console.WriteLine("Estudiante insertado correctamente");
                //}
                //else
                //{
                //    Console.WriteLine("Estudiante no ingresado");
                //} 
                #endregion

                //Console.WriteLine("Ingrese Nro cédula"));
                //Console.ReadLine(ccDigitada);

                #region [ Obtener estudiante ]
                //Estudiante estudianteBuscar = fachadaEstudiante.RetornarEstudiante(80);

                //if (estudianteBuscar != null)
                //{
                //    Console.WriteLine($"{estudianteBuscar.Nombre} {estudianteBuscar.Apellidos}");
                //}

                //else
                //{
                //    Console.WriteLine("Estudiante no existe");
                //}
                #endregion



                //foreach (var estudiante in fachadaEstudiante.RetornarEstudiantes())
                //{
                //    Console.WriteLine($"{estudiante.Nombre} {estudiante.Apellidos}");
                //}
                Console.WriteLine("Digite el estudiante a eliminar");
                string cc = Console.ReadLine();
                int cedula = int.Parse(cc);
                //Estudiante estudianteEliminar = fachadaEstudiante.EliminarEstudiante(cedula);

                //if (estudianteEliminar != null)
                //{
                //    Console.WriteLine("Estudiante eliminado con éxito!");
                //}

                //else
                //{
                //    Console.WriteLine("Estudiante no existe");
                //}


                //foreach (var ciudad in fachadaCiudad.RetornarCiudad())
                //{
                //    Console.WriteLine($"{ciudad.Id} {ciudad.NombreCiudad}");
                //}

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadKey();
            }


        }
    }
}
