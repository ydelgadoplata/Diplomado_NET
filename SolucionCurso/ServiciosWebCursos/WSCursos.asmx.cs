﻿using AccesoDatos.DTOs;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ServiciosWebCursos
{
    /// <summary>
    /// Summary description for WSCursos
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSCursos : System.Web.Services.WebService
    {
        private FachadaCiudad _fachadaCiudad = new FachadaCiudad();
        

        [WebMethod]
        public string Saludo_Servicios()
        {
            return "Servicios Web!!! a aprender";
        }

        [WebMethod]
        public int Sumar(int i, int j)
        {
            int resultadoSuma = i + j;
            return resultadoSuma;
        }

        [WebMethod]
        public List<CiudadDTO> ListadoCiudades()
        {
            return _fachadaCiudad.RetornarCiudadesDTO();
        }
    }
}
