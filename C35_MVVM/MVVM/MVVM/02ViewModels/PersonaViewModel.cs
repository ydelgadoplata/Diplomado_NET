﻿using MVVM._00Model;
using MVVM._03Servicios;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MVVM._02ViewModels
{
    public class PersonaViewModel : Persona
    {
        public ObservableCollection<Persona> Personas { get; set; }

        private PersonaServicio _personaServicio;

        private Persona _persona;

        public Command GuardarCommand { get; set; }


        public PersonaViewModel()
        {
            if (_personaServicio == null)
            {
                _personaServicio = new PersonaServicio();
            }

            Personas = _personaServicio.RetornarPersona();

            GuardarCommand = new Command(async () => await Guardar(), () => !EstaOcupado);
        }

        public async Task Guardar()
        {
            EstaOcupado = true;

            _persona = new Persona
            {
                Id = this.Id,
                Nombre = this.Nombre,
                Apellido = this.Apellido,
                Edad = this.Edad
            };

            _personaServicio.GuardarPersona(_persona);

            await Task.Delay(2000);

            EstaOcupado = false;
        }

    }
}
