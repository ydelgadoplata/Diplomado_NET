﻿using MVVM._02ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MVVM._01View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PersonasList : ContentPage
	{
        private PersonaViewModel _personaViewModel;
        
        public PersonasList ()
		{
			InitializeComponent ();

            if (_personaViewModel == null)
            {
                _personaViewModel = new PersonaViewModel();
            }

            BindingContext = _personaViewModel;

            this.Title = "Listado Personas";

            var toolBarItem = new ToolbarItem
            {
                Text = "+"
            };

            toolBarItem.Clicked += ToolBarItem_Clicked;

            ToolbarItems.Add(toolBarItem);
		}

        private async void ToolBarItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PagePersona());
        }
    }
}