﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace MVVM._00Model
{
    public class Persona : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChange([CallerMemberName]string nombre = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nombre));
        }

        private bool _estaOcupado;

        public bool EstaOcupado
        {
            get { return _estaOcupado; }
            set { _estaOcupado = value; OnPropertyChange(); }
        }


        private string _id;

        public string Id
        {
            get { return _id; }
            set { _id = value; OnPropertyChange(); }
        }

        private string _nombre;

        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; OnPropertyChange(); OnPropertyChange(nameof(NombreCompleto)); }
        }

        private string _apellido;

        public string Apellido
        {
            get { return _apellido; }
            set { _apellido = value; OnPropertyChange(); OnPropertyChange(nameof(NombreCompleto)); }
        }

        private int _edad;

        public int Edad
        {
            get { return _edad; }
            set { _edad = value; OnPropertyChange(); }
        }

        public string NombreCompleto
        {
            get { return $"{Nombre} {Apellido}"; }
        }

    }
}
