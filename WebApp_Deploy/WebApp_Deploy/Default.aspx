﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApp_Deploy.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <%--<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">--%>
    <link href="../Content/bootstrap.css" rel="stylesheet" />
    <title>Deploy Diplomado .NET I</title>
</head>
<body>
    <form id="form1" runat="server" class="offset-md-1 col-md-10">
        <div class="mt-1">
            <div class="jumbotron mt-3 bg-light">
                <h1 class="display-4 text-center">DIPLOMADO .NET I</h1>
            </div>
            <div class="mt-1">
                <div class="card">
                    <div class="card-header bg-info text-white">
                        Digite sus datos
                    </div>
                    <div class="card-body">
                        <!-- Nombre -->
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="txtNombre">
                                Nombre: 
                        <%--<asp:Literal Text="<%$Resources:multilenguaje, nombre%>" runat="server" />--%>
                            </label>
                            <div class="col-md-8">
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtNombre" Placeholder="Ingrese nombre" />
                            </div>
                            <div class="col-md-2">
                                <%--<asp:CompareValidator ErrorMessage="Digite sólo caracteres" CssClass="alert-danger" ControlToValidate="txtNombre" Operator="DataTypeCheck" Type="String" runat="server" />--%>
                                <asp:RequiredFieldValidator ErrorMessage="Nombre requerido" ControlToValidate="txtNombre" runat="server" ID="rfvNombre" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                                <asp:RegularExpressionValidator ErrorMessage="Sólo caractéres" ControlToValidate="txtNombre" ValidationExpression="^[ A-Za-zÑñ]*$" CssClass="alert-danger" runat="server" />
                            </div>
                        </div>
                        <!-- Fecha Nacimiento -->
                        <div class="form-group row">
                            <label class="col-md-2 mt-auto col-form-label align-items-center">
                                Fecha Nacimiento: 
                            </label>
                            <div class="col-md-2">
                                <label class="col-form-label">Día:</label>
                                <asp:DropDownList runat="server" ID="ddlDia" CssClass="form-control">
                                </asp:DropDownList>
                                <%--<asp:RequiredFieldValidator ControlToValidate="ddlDia" ErrorMessage="*" ID="rfvDia" CssClass="alert-danger" Display="Dynamic" runat="server" />--%>
                            </div>
                            <div class="col-md-2">
                                <label class="col-form-label">Mes:</label>
                                <asp:DropDownList runat="server" ID="ddlMes" CssClass="form-control">
                                </asp:DropDownList>
                                <%--<asp:RequiredFieldValidator ControlToValidate="ddlMes" ErrorMessage="*" ID="rfvMes" CssClass="alert-danger" Display="Dynamic" runat="server" />--%>
                            </div>
                            <div class="col-md-2">
                                <label class="col-form-label">Año:</label>
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtYear" Placeholder="1982,1990..." MaxLength="4" />
                                <asp:CompareValidator ErrorMessage="Sólo dígitos" CssClass="alert-danger" ControlToValidate="txtYear" Operator="DataTypeCheck" Type="Integer" runat="server" />
                                <asp:RequiredFieldValidator ErrorMessage="*" ControlToValidate="txtYear" runat="server" ID="RequiredFieldValidator1" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger"/>
                            </div>
                            <%--<div class="col-md-2">
                                <asp:CompareValidator ErrorMessage="Sólo 4 dígitos" CssClass="alert-danger" ControlToValidate="txtYear" Operator="DataTypeCheck" Type="Integer" runat="server" />
                                <asp:RequiredFieldValidator ErrorMessage="Campo requerido" ControlToValidate="txtYear" runat="server" ID="rfvYear" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                            </div>--%>
                        </div>
                        <div class="form-group row" style="justify-content: center">
                            <div class="col-md-4">
                                <asp:Button Text="Próximo cumpleaños?" runat="server" ID="btnCumples" CssClass="btn btn-info btn-block" OnClick="btnCumples_Click" />
                            </div>
                        </div>
                        <div class="form-group row" style="align-items: center">
                            <div class="col-12">
                                <asp:Label Text="" runat="server" ID="lblInfo" CssClass="text-success text-body" />
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </form>
    <script src="<%= Page.ResolveUrl("~/Scripts/jquery-3.3.1.js") %>"></script>
    <%--<script src="<%= Page.ResolveUrl("~/Scripts/popper.js") %>"></script>--%>
    <script src="<%= Page.ResolveUrl("~/Scripts/bootstrap.js") %>"></script>
</body>
</html>
