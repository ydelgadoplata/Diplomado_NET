﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp_Deploy
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region [ Llenar combo de día ]
                ddlDia.Items.Insert(0, new ListItem("--", "0"));
                for (int i = 1; i < 32; i++)
                {
                    ddlDia.Items.Insert(i, new ListItem($"{i}", $"{i}"));
                }
                #endregion

                #region [ Llenar combo de mes ]
                ddlMes.Items.Insert(0, new ListItem("--", "0"));
                for (int i = 1; i < 13; i++)
                {
                    ddlMes.Items.Insert(i, new ListItem($"{i}", $"{i}"));
                }
                #endregion

                lblInfo.CssClass = "text-success";
                lblInfo.Text = " ";
                ddlDia.SelectedValue = ddlMes.SelectedValue = "0";
            }
            
        }

        protected void btnCumples_Click(object sender, EventArgs e)
        {
            DateTime FechaActual = DateTime.Now;
            int DiaAct = DateTime.Today.Day;
            int MesAct = DateTime.Today.Month;
            int YearAct = DateTime.Today.Year;

            if (Page.IsValid)
            {
                if (txtYear.Text != " " && ddlMes.SelectedValue != "0" && ddlDia.SelectedValue != "0")
                {
                    DateTime FechaNacimiento = new DateTime(Convert.ToInt32(txtYear.Text), Convert.ToInt32(ddlMes.SelectedValue), Convert.ToInt32(ddlDia.SelectedValue));

                    int TotalEdad = YearAct - FechaNacimiento.Year;
                    int MesesFaltantes = MesAct - FechaNacimiento.Month;
                    int DiasFaltantes = DiaAct - FechaNacimiento.Day;


                    if (TotalEdad != 0)
                    {
                        if (TotalEdad < 0)
                        {
                            lblInfo.CssClass = "text-danger";
                            lblInfo.Text = "Ud. no ha nacido. Buen Intento!";
                            txtNombre.Text = txtYear.Text = null;
                            ddlDia.SelectedValue = ddlMes.SelectedValue = "0";
                        }
                        if (TotalEdad > 0)
                        {
                            if (MesesFaltantes == 0)
                            {
                                if (DiasFaltantes == 0)
                                {
                                    lblInfo.CssClass = "text-success";
                                    lblInfo.Text = $"FELIZ CUMPLEAÑOS {txtNombre.Text}, {TotalEdad} años!!!";
                                    txtNombre.Text = txtYear.Text = null;
                                    ddlDia.SelectedValue = ddlMes.SelectedValue = "0";
                                }
                                if (DiasFaltantes > 0)
                                {
                                    lblInfo.CssClass = "text-success";
                                    lblInfo.Text = $"{txtNombre.Text}, tienes {TotalEdad} años y te queda (1) año y {DiasFaltantes} días para tu próximo cumpleaños!";
                                    txtNombre.Text = txtYear.Text = null;
                                    ddlDia.SelectedValue = ddlMes.SelectedValue = "0";
                                }
                                if (DiasFaltantes < 0)
                                {
                                    lblInfo.CssClass = "text-success";
                                    lblInfo.Text = $"{txtNombre.Text}, tienes {TotalEdad - 1} años y te quedan {-DiasFaltantes} días, para tu próximo cumpleaños!";
                                    txtNombre.Text = txtYear.Text = null;
                                    ddlDia.SelectedValue = ddlMes.SelectedValue = "0";
                                }
                                //else
                                //{
                                //    return;
                                //}
                            }
                            //else
                            //{
                            //    return;
                            //}
                            if (MesesFaltantes > 0)
                            {
                                if (DiasFaltantes > 0)
                                {
                                    lblInfo.CssClass = "text-success";
                                    lblInfo.Text = $"{txtNombre.Text}, tienes {TotalEdad} años y te quedan {Convert.ToString(12 - MesesFaltantes)} meses y {DiasFaltantes} días para tu próximo cumpleaños!";
                                    txtNombre.Text = txtYear.Text = null;
                                    ddlDia.SelectedValue = ddlMes.SelectedValue = "0";
                                }
                                if (DiasFaltantes < 0)
                                {
                                    lblInfo.CssClass = "text-success";
                                    lblInfo.Text = $"{txtNombre.Text}, tienes {TotalEdad} años y te quedan {Convert.ToString(12 - MesesFaltantes)} meses y {-DiasFaltantes} días para tu próximo cumpleaños!";
                                    txtNombre.Text = txtYear.Text = null;
                                    ddlDia.SelectedValue = ddlMes.SelectedValue = "0";
                                }
                                if (DiasFaltantes == 0)
                                {
                                    lblInfo.CssClass = "text-success";
                                    lblInfo.Text = $"{txtNombre.Text}, tienes {TotalEdad} años y te quedan {Convert.ToString(12 - MesesFaltantes)} meses para tu próximo cumpleaños!";
                                    txtNombre.Text = txtYear.Text = null;
                                    ddlDia.SelectedValue = ddlMes.SelectedValue = "0";
                                }
                                //else
                                //{
                                //    return;
                                //}
                            }
                            if (MesesFaltantes < 0)
                            {
                                if (DiasFaltantes > 0)
                                {
                                    lblInfo.CssClass = "text-success";
                                    lblInfo.Text = $"{txtNombre.Text}, tienes {TotalEdad - 1} años y te quedan {Convert.ToString(-MesesFaltantes)} meses y {DiasFaltantes} días para tu próximo cumpleaños!";
                                    txtNombre.Text = txtYear.Text = null;
                                    ddlDia.SelectedValue = ddlMes.SelectedValue = "0";
                                }
                                //else
                                //{
                                //    return;
                                //}
                                if (DiasFaltantes < 0)
                                {
                                    lblInfo.CssClass = "text-success";
                                    lblInfo.Text = $"{txtNombre.Text}, tienes {TotalEdad - 1} años y te quedan {Convert.ToString(-MesesFaltantes)} meses y {-DiasFaltantes} días, para tu próximo cumpleaños!";
                                    txtNombre.Text = txtYear.Text = null;
                                    ddlDia.SelectedValue = ddlMes.SelectedValue = "0";
                                }
                                //else
                                //{
                                //    return;
                                //}
                                if (DiasFaltantes == 0)
                                {
                                    lblInfo.CssClass = "text-success";
                                    lblInfo.Text = $"{txtNombre.Text}, tienes {TotalEdad - 1} años y te quedan {Convert.ToString(-MesesFaltantes)} meses para tu próximo cumpleaños!";
                                    txtNombre.Text = txtYear.Text = null;
                                    ddlDia.SelectedValue = ddlMes.SelectedValue = "0";
                                }
                            }
                            //else
                            //{
                            //    return;
                            //}
                        }
                        //else
                        //{
                        //    return;
                        //}
                    }
                    else
                    {
                        lblInfo.CssClass = "text-danger";
                        lblInfo.Text = "Debe escribir año de nacimiento";
                        txtNombre.Text = txtYear.Text = null;
                        ddlDia.SelectedValue = ddlMes.SelectedValue = "0";
                    }
                }
                else
                {
                    lblInfo.CssClass = "text-danger";
                    lblInfo.Text = "Debe seleccionar día y mes de nacimiento";
                    txtNombre.Text = txtYear.Text = null;
                    ddlDia.SelectedValue = ddlMes.SelectedValue = "0";
                }
            }
            else
            {
                lblInfo.CssClass = "text-danger";
                lblInfo.Text = "Debe seleccionar día, mes y escribir año de nacimiento";
                txtNombre.Text = txtYear.Text = null;
                ddlDia.SelectedValue = ddlMes.SelectedValue = "0";
            }
        }
    }
}
