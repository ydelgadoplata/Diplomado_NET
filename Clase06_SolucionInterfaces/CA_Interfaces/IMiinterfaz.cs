﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Interfaces
{
    public interface IMiInterfaz
    {
        void MetodoDeLaInterfaz();

        string IngresoDeTexto(string texto);
   
    }
}
