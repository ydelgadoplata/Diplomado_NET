﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Interfaces
{
    public class MiClase : IMiInterfaz
    {
        public string IngresoDeTexto(string texto)
        {
            Console.WriteLine("Escriba algo...");
            texto = Console.ReadLine();
            return texto;
        }

        public void MetodoDeLaInterfaz()
        {
            Console.WriteLine("Método de la interfaz implementada");
        }

    }
}
