﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProyectoCalculadora;

namespace ProyectoCalculadoraTest
{
    [TestClass]
    public class CalculadoraTest
    {
        [TestMethod]
        public void Sumar_2_3()
        {
            Calculadora calculadora = new Calculadora();
            int resultado = calculadora.Sumar(2, 3);
            Assert.AreEqual(5, resultado);
        }

        [TestMethod]
        public void Dividir_2numeros()
        {
            Calculadora calculadora = new Calculadora();
            int resultado = calculadora.Dividir(4, 2);
            Assert.AreEqual(2, resultado);
        }

        [TestMethod]
        //[ExpectedException(typeof(DivideByZeroException))]
        public void Dividir_0()
        {

            Calculadora calculadora = new Calculadora();
            int resultado = calculadora.Dividir(4, 0);
            Assert.ThrowsException<DivideByZeroException>(Action<T>);
        }
    }
}
