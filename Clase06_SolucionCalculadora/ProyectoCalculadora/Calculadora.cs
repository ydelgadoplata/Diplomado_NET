﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoCalculadora
{
    public class Calculadora
    {
        public int Sumar(int valor1, int valor2)
        {
            return valor1 + valor2;
        }

        public int Dividir(int valor1, int valor2)
        {
            return valor1 / valor2;
        }
    }
}
