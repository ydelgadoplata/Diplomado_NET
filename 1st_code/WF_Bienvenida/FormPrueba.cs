﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Bienvenida
{
    public partial class FormPrueba : Form
    {

        public FormPrueba()
        {
            InitializeComponent();
            btnClose.Enabled = false;
            label1.Text = "Si quiere cerrar la ventana, utilice el botón.";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //MessageBox.Show("Ahora sabemos cómo hacerlo gráfico!");
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("El botón para cerrar la ventana ha sido habilitado");
            btnClose.Enabled = true;
            label1.Text = "Ahora puede cerrar la ventana, Gracias!";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FormPrueba.ActiveForm.Close();
        }
    }
}